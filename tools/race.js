
var canvas;
var context;
var backgroundPattern;
var CANVAS_ANGLE_OFFSET = 90;
var CANVAS_PADDING = 100;
var TRACK_LINE_WIDTH = 80;
var trackLeft;
var trackRight;
var trackTop;
var trackBottom;

function toDegrees (angle) 
{
  return angle * (180 / Math.PI);
}

function toRadians (angle) 
{
  return angle * (Math.PI / 180);
}

function initCanvas()
{
  canvas = document.getElementById("myCanvas")
  context = canvas.getContext('2d');

  // Load the background image
  var backgroundImage = new Image();
  backgroundImage.onload = function() 
  {
    backgroundPattern = context.createPattern(backgroundImage, 'repeat');
  };

  backgroundImage.src = 'bg.jpg';
}

function drawTrack()
{
  var jsontext = $("#json")[0].value;
  var track = JSON.parse(jsontext).track;
  
  draw(track);
}

function draw(track)
{
  context.save();

  // Now we need more info for the track
  trackLeft = 1000;
  trackRight = -1000;
  trackTop = 1000;
  trackBottom = -1000;
  var startAngle;
  var startX;
  var startY;
  
  for (var i=0; i<track.pieces.length; ++i)
  {
    var piece = track.pieces[i];

    if (i == 0)
    {
        startAngle = track.startingPoint.angle - CANVAS_ANGLE_OFFSET;
        startX = track.startingPoint.position.x;
        startY = track.startingPoint.position.y;
    }
    else
    {
        startAngle = track.pieces[i-1].endAngle;
        startX = track.pieces[i-1].endPosition.x;
        startY = track.pieces[i-1].endPosition.y;
    }

    // This will set in our track where the pieces are in the canvas
    // Do this before drawing!
    // This is needed for drawing the cars on the track easily!
    calculatePiecePosition(piece, startX, startY, startAngle);
  }

  // Now set up our canvas
  prepareCanvas();

  // Now draw the track background
  context.save();
  context.strokeStyle = "grey";
  context.lineWidth = TRACK_LINE_WIDTH;
  context.lineCap="round";
  context.beginPath();

  for (var i=0; i<track.pieces.length; ++i)
  {
    var piece = track.pieces[i];
    drawPiece(piece, 0);
  }

  context.stroke();
  context.restore();

  // Now draw the lanes
  context.beginPath();

  for (var i=0; i<track.pieces.length; ++i)
  {
    var piece = track.pieces[i];
    for (var j = 0; j < track.lanes.length; ++j) 
    {
        if (j != 0 && piece.switch)
        {
            drawSwitch(piece, track.lanes[j].distanceFromCenter, track.lanes[j-1].distanceFromCenter);
        }
        drawPiece(piece, track.lanes[j].distanceFromCenter);
    }
  }
  
  context.stroke();

  // Now draw the start line
  drawStartLine(track.startingPoint);

  // Now draw the title in the upper left
  drawTitle(track.name);
  context.restore();
}

function calculatePiecePosition(piece, curX, curY, curAngle)
{
    // Update our object. We will need these values when drawing cars!
    if (!piece.endPosition)
    {
      // Start positions are easy
      piece.startPosition = {};
      piece.startPosition.x = curX;
      piece.startPosition.y = curY;
      piece.startAngle = curAngle;

      // Now figure out where this track "ends"
      var endX;
      var endY;
      var endAngle;

      if (!piece.length)
      {
        // A bend
        var tangent = curAngle + (piece.angle < 0 ? -CANVAS_ANGLE_OFFSET : CANVAS_ANGLE_OFFSET);
        var centerX = curX + Math.cos(toRadians(tangent)) * piece.radius;
        var centerY = curY + Math.sin(toRadians(tangent)) * piece.radius;
        endX = centerX + Math.cos(toRadians(tangent - 180 + piece.angle)) * piece.radius;
        endY = centerY + Math.sin(toRadians(tangent - 180 + piece.angle)) * piece.radius;

        endAngle = curAngle + piece.angle;

        // To update the track positioning, we additionally check the center point on curve
        var midX = centerX + Math.cos(toRadians(tangent - 180 + piece.angle / 2)) * piece.radius;
        var midY = centerY + Math.sin(toRadians(tangent - 180 + piece.angle / 2)) * piece.radius;
        updateTrackPosition(midX, midY, TRACK_LINE_WIDTH/2);
      }
      else
      {
        // a straight track
        endX = curX + Math.cos(toRadians(curAngle)) * piece.length;
        endY = curY + Math.sin(toRadians(curAngle)) * piece.length;
        endAngle = curAngle;
      }

      piece.endPosition = {};
      piece.endPosition.x = endX;
      piece.endPosition.y = endY;
      piece.endAngle = endAngle;

      // Update the track position
      updateTrackPosition(curX, curY, TRACK_LINE_WIDTH/2);
      updateTrackPosition(endX, endY, TRACK_LINE_WIDTH/2);
    }
}

function updateTrackPosition(curX, curY, offset)
{
      // Now figure out our track position!
      trackLeft = curX - offset < trackLeft ? curX - offset : trackLeft;
      trackRight = curX + offset > trackRight ? curX + offset : trackRight;
      trackBottom = curY + offset > trackBottom ? curY + offset : trackBottom;
      trackTop = curY - offset < trackTop ? curY - offset : trackTop;
}

function prepareCanvas()
{
  // Set the size
  var width = trackRight - trackLeft + CANVAS_PADDING;
  var left = trackLeft - CANVAS_PADDING/2;
  var height = trackBottom - trackTop + CANVAS_PADDING;
  var top = trackTop - CANVAS_PADDING/2;
  canvas.width = width;
  canvas.style.width = width;
  canvas.height = height;
  canvas.style.height = height;

  // Make the center the center of the track
  context.translate(-left, -top);

  // clear and apply background
  context.save();
  context.rect(left, top, width, height);
  context.fillStyle = backgroundPattern;
  context.fill();
  context.restore();
}

function getPieceXWithOffset(piece, offset)
{
    var tangent = piece.startAngle + CANVAS_ANGLE_OFFSET;
    return piece.startPosition.x + Math.cos(toRadians(tangent)) * offset;
}

function getPieceYWithOffset(piece, offset)
{
    var tangent = piece.startAngle + CANVAS_ANGLE_OFFSET;
    return piece.startPosition.y + Math.sin(toRadians(tangent)) * offset;
}

function getPieceEndXWithOffset(piece, offset)
{
    var tangent = piece.endAngle + CANVAS_ANGLE_OFFSET;
    return piece.endPosition.x + Math.cos(toRadians(tangent)) * offset;
}

function getPieceEndYWithOffset(piece, offset)
{
    var tangent = piece.endAngle + CANVAS_ANGLE_OFFSET;
    return piece.endPosition.y + Math.sin(toRadians(tangent)) * offset;
}

function drawPiece(piece, offset)
{
    var curX = getPieceXWithOffset(piece, offset);
    var curY = getPieceYWithOffset(piece, offset);

    if (!piece.length)
    {
      // Are we turning left or right?
      var counterClockwise = piece.angle < 0;

      // Adjust the offset depending how we are drawing
      var adjstedOffset = counterClockwise ? offset : -offset;

      // Now find the center
      var tangent = piece.startAngle + (counterClockwise ? -CANVAS_ANGLE_OFFSET : CANVAS_ANGLE_OFFSET);
      var centerX = curX + Math.cos(toRadians(tangent)) * (piece.radius + adjstedOffset);
      var centerY = curY + Math.sin(toRadians(tangent)) * (piece.radius + adjstedOffset);

      // Find the angle from center to the current point
      var curPointAngle = toRadians(tangent - 180);
      var endPointAngle = toRadians(piece.startAngle + piece.angle + (counterClockwise ? -270 : -90));
      
      // Draw the curve
      context.moveTo(curX, curY);
      context.arc(centerX, centerY, piece.radius + adjstedOffset, curPointAngle, endPointAngle, counterClockwise);
      context.moveTo(endX, endY);
    }
    else
    {
      // we are at a straight away
      var endX = getPieceEndXWithOffset(piece, offset);
      var endY = getPieceEndYWithOffset(piece, offset);

      // Draw the line
      context.moveTo(curX, curY);
      context.lineTo(endX, endY);
    }
}

function drawSwitch(piece, position, otherPosition)
{
    var startX1 = getPieceXWithOffset(piece, position);
    var startY1 = getPieceYWithOffset(piece, position);
    var startX2 = getPieceXWithOffset(piece, otherPosition);
    var startY2 = getPieceYWithOffset(piece, otherPosition);
    var endX1 = getPieceEndXWithOffset(piece, position);
    var endY1 = getPieceEndYWithOffset(piece, position);
    var endX2 = getPieceEndXWithOffset(piece, otherPosition);
    var endY2 = getPieceEndYWithOffset(piece, otherPosition);

    context.moveTo(startX1, startY1);
    context.lineTo(endX2, endY2);
    context.moveTo(startX2, startY2);
    context.lineTo(endX1, endY1);
}

function drawStartLine(start)
{
  // Make the start line 60 long,
  var lineRadius = 30;

  // Calculate end position
  var tangent = start.angle;
  var startX = start.position.x + Math.cos(toRadians(tangent)) * lineRadius;
  var startY = start.position.y + Math.sin(toRadians(tangent)) * lineRadius;
  var endX = start.position.x + Math.cos(toRadians(tangent - 180)) * lineRadius;
  var endY = start.position.y + Math.sin(toRadians(tangent - 180)) * lineRadius;

  // Now do the drawing
  context.save();
  context.beginPath();
  context.lineWidth=5;
  context.strokeStyle="yellow";
  context.moveTo(startX, startY);
  context.lineTo(endX, endY);
  context.stroke();
  context.restore()
}

function drawTitle(name)
{
    context.font="32px Georgia";
    context.fillText(name, trackLeft, trackTop - CANVAS_PADDING/2 + 30);
}