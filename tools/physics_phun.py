import json
import math
import numpy
from numpy import arange,array,ones,linalg
from pylab import plot,show

data = [x.strip().split(",") for x in open("telem_miss.csv").readlines()]

trackSegs = set(['2','3','4'])

def deconflict_angle(a, av):
    sK = 0.0075
    dK = 0.1
    fS = (0 - a) * sK
    av += fS + dK * av * -1.0
    return a + av


keypoints = [[int(x[0]), int(x[2]), int(x[3]), int(float(x[8]) * 10), float(x[8]), float(x[10]), float(x[11]), float(x[12]), deconflict_angle(float(x[11]), float(x[12])), float(x[10])] for x in data if x[1] == '1' and x[2] in trackSegs and float(x[8]) > 0]

ticks = {}

for k in keypoints:
    if k[3] not in ticks:
        ticks[k[3]] = set()

    ticks[k[3]].add(k[0])

firstTick = {}
tL = {}

for t in ticks.keys():
    firstTick[t] = min(ticks[t])
    tL[t] = len(ticks[t])

goodVelocities = set([x for x in tL.keys() if tL[x] > 15 and x > 40])

keypoints = [x for x in keypoints if x[3] in goodVelocities]

for k in keypoints:
    k[0] -= firstTick[k[3]]

with open("/home/charles/Dropbox/hwo/tm_out.csv", "w") as out:
    out.write("Tick,PieceID,VBucket,Angle,Deconflicted\n")

    for k in keypoints:
        out.write("%d,%d,%d,%f,%f\n" % (k[0], k[1], k[3], k[6], k[8]))

for v in goodVelocities:
    y = []
    x = []

    radius = 0
    for k in keypoints:
        if k[3] == v and k[1] == 4:
            x.append(k[0])
            y.append(k[7])
            radius = k[9]



    A = array([ x, ones(len(x))])
    w = linalg.lstsq(A.T,y)[0][1]

    print (v, radius, w, max(y))