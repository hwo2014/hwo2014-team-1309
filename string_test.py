import math

vel = -0.277231999999979
pos = 31.490775

sK = 0.00833
dK = 0.1

pos = 0.132672
vel = 0.119709 - 0.132672

targets=[float(x) for x in """0.119709
0.096987
0.065451
0.026019
-0.020422
-0.073013
-0.130934
-0.193403
-0.259679
-0.329062
-0.400895
-0.474567
-0.549508
-0.625195
-0.701147
-0.776925
-0.852133
-0.926416
-0.999459
-1.070983
-1.140745
-1.20854
-1.274191
-1.337556
-1.398518
-1.45699
-1.512907
-1.56623
-1.616938
-1.665032
-1.710527
-1.753456
-1.793865
-1.831811
-1.867364
-1.900599
-1.931602
-1.960464
-1.987281
-2.012151
-2.035179
-2.056466
-2.076119""".split("\n")]

minCk = 0
minFk = 0
minerr = 100000000

for fkc in range(0,1000):
    for skc in range(0,1):
        pos = 0.132672
        vel = 0.119709 - 0.132672
        erracc = 0

        fC = fkc / 100000.0 * -1.0

        for x in range(0,40):
            pos += vel
            #print("%f,%f"%(pos-targets[x],pos))
            erracc += (pos-targets[x]) ** 2

            fS = (0 - pos) * sK

            vel += fS
            vel += dK * vel * -1.0
            vel += fC

        if erracc < minerr:
            minCk = fC
            minFk = 0
            minerr = erracc

    if fkc % 10 == 0:
        print fkc
        print minerr

print minFk
print minCk
print minerr
