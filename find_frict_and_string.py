import math
#Test for git push
vel = -0.277231999999979
pos = 31.490775

sK = 0.001
fK = 0.005

targets=[float(x) for x in """31.213543
30.729933
30.06421
29.239577
28.278111
27.200705
26.027035
24.775529
23.463357
22.106428
20.719393
19.315666
17.907444
16.505738
15.12041
13.760212
12.432832
11.144944
9.902258
8.709573
7.570835
6.489189
5.467039
4.506102
3.607462
2.77163
1.998594
1.287873
0.638564
0.049397
-0.481223""".split("\n")]

minFk = 0
minSk = 0
minerr = 100000000

for fkc in range(0,1000):
    for skc in range(0,1000):

        erracc = 0
        vel = -0.277231999999979
        pos = 31.490775
        fK = fkc / 1000.0
        sK = skc / 10000.0        

        for x in range(0,30):
            pos += vel
            #print("%f,%f"%(pos-targets[x],pos))

            erracc += (pos-targets[x]) ** 2


            fS = (0 - pos) * sK

            vel += fS + fK * vel * -1.0

        if erracc < minerr:
            minFk = fK
            minSk = sK
            minerr = erracc

    if fkc % 10 == 0:
        print fkc
        print minerr

print minerr
