var canvas;
var context;
var carCanvas;
var carContext;
var explosionCanvas;
var explosionContext;
var racecars = [];
var backgroundPattern;
var CANVAS_ANGLE_OFFSET = 90;
var CANVAS_PADDING = 100;
var TRACK_LINE_WIDTH = 60;
var trackLeft;
var trackRight;
var trackTop;
var trackBottom;
var _track;

function toDegrees(angle) {
    return angle * (180 / Math.PI);
}

function toRadians(angle) {
    return angle * (Math.PI / 180);
}


function initCanvas() {
    canvas = document.getElementById("myCanvas")
    context = canvas.getContext('2d');
    carCanvas = document.getElementById("vehicleCanvas");
    carContext = carCanvas.getContext('2d');
    explosionCanvas = document.getElementById("explosionCanvas");
    explosionContext = explosionCanvas.getContext('2d');

    // Load the background image
    var backgroundImage = new Image();
    backgroundImage.onload = function() {
        backgroundPattern = context.createPattern(backgroundImage, 'repeat');
    };

    backgroundImage.src = 'bg.jpg';

    Telemetry.init();

}

function drawTrack() {
    var jsontext = $("#json")[0].value;
    var track = JSON.parse(jsontext).track;

    draw(track);
}

function draw(track) {
    _track = track;
    context.save();

    // Now we need more info for the track
    trackLeft = 1000;
    trackRight = -1000;
    trackTop = 1000;
    trackBottom = -1000;
    var startAngle;
    var startX;
    var startY;

    for (var i = 0; i < track.pieces.length; ++i) {
        var piece = track.pieces[i];

        if (i == 0) {
            startAngle = track.startingPoint.angle - CANVAS_ANGLE_OFFSET;
            startX = track.startingPoint.position.x;
            startY = track.startingPoint.position.y;
        } else {
            startAngle = track.pieces[i - 1].endAngle;
            startX = track.pieces[i - 1].endPosition.x;
            startY = track.pieces[i - 1].endPosition.y;
        }

        // This will set in our track where the pieces are in the canvas
        // Do this before drawing!
        // This is needed for drawing the cars on the track easily!
        calculatePiecePosition(piece, startX, startY, startAngle);
    }

    // Now set up our canvas
    prepareCanvas();

    // Now draw the track background
    context.save();
    context.strokeStyle = "grey";
    context.lineWidth = TRACK_LINE_WIDTH;
    context.lineCap = "round";
    context.beginPath();

    for (var i = 0; i < track.pieces.length; ++i) {
        var piece = track.pieces[i];
        drawPiece(piece, 0);
    }

    context.stroke();
    context.restore();

    // Now draw the lanes
    context.beginPath();

    for (var i = 0; i < track.pieces.length; ++i) {
        var piece = track.pieces[i];
        for (var j = 0; j < track.lanes.length; ++j) {
            if (j != 0 && piece.
                switch) {
                drawSwitch(piece, track.lanes[j].distanceFromCenter, track.lanes[j - 1].distanceFromCenter);
            }
            drawPiece(piece, track.lanes[j].distanceFromCenter);
        }
    }

    context.stroke();

    for (var i = 0; i < track.pieces.length; ++i) {
        var piece = track.pieces[i];
        labelPiece(piece, 0, '' + i);
    }

    // Now draw the start line
    drawStartLine(track.startingPoint);

    // Now draw the title in the upper left
    drawTitle(track.name);
    context.restore();
}

function calculatePiecePosition(piece, curX, curY, curAngle) {
    // Update our object. We will need these values when drawing cars!
    if (!piece.endPosition) {
        // Start positions are easy
        piece.startPosition = {};
        piece.startPosition.x = curX;
        piece.startPosition.y = curY;
        piece.startAngle = curAngle;

        // Now figure out where this track "ends"
        var endX;
        var endY;
        var endAngle;

        if (!piece.length) {
            // A bend
            var tangent = curAngle + (piece.angle < 0 ? -CANVAS_ANGLE_OFFSET : CANVAS_ANGLE_OFFSET);
            var centerX = curX + Math.cos(toRadians(tangent)) * piece.radius;
            var centerY = curY + Math.sin(toRadians(tangent)) * piece.radius;
            endX = centerX + Math.cos(toRadians(tangent - 180 + piece.angle)) * piece.radius;
            endY = centerY + Math.sin(toRadians(tangent - 180 + piece.angle)) * piece.radius;

            endAngle = curAngle + piece.angle;

            // To update the track positioning, we additionally check the center point on curve
            var midX = centerX + Math.cos(toRadians(tangent - 180 + piece.angle / 2)) * piece.radius;
            var midY = centerY + Math.sin(toRadians(tangent - 180 + piece.angle / 2)) * piece.radius;
            piece.midX = midX;
            piece.midY = midY;
            updateTrackPosition(midX, midY, TRACK_LINE_WIDTH / 2);
        } else {
            // a straight track
            endX = curX + Math.cos(toRadians(curAngle)) * piece.length;
            endY = curY + Math.sin(toRadians(curAngle)) * piece.length;
            endAngle = curAngle;
        }

        piece.endPosition = {};
        piece.endPosition.x = endX;
        piece.endPosition.y = endY;
        piece.endAngle = endAngle;

        // Update the track position
        updateTrackPosition(curX, curY, TRACK_LINE_WIDTH / 2);
        updateTrackPosition(endX, endY, TRACK_LINE_WIDTH / 2);
    }
}

function updateTrackPosition(curX, curY, offset) {
    // Now figure out our track position!
    trackLeft = curX - offset < trackLeft ? curX - offset : trackLeft;
    trackRight = curX + offset > trackRight ? curX + offset : trackRight;
    trackBottom = curY + offset > trackBottom ? curY + offset : trackBottom;
    trackTop = curY - offset < trackTop ? curY - offset : trackTop;
}

function prepareCanvas() {
    // Set the size
    var width = trackRight - trackLeft + CANVAS_PADDING;
    var left = trackLeft - CANVAS_PADDING / 2;
    var height = trackBottom - trackTop + CANVAS_PADDING;
    var top = trackTop - CANVAS_PADDING / 2;
    canvas.width = width;
    canvas.style.width = width;
    canvas.height = height;
    canvas.style.height = height;
    carCanvas.width = width;
    carCanvas.style.width = width;
    carCanvas.height = height;
    carCanvas.style.height = height;
    explosionCanvas.width = width;
    explosionCanvas.style.width = width;
    explosionCanvas.height = height;
    explosionCanvas.style.height = height;

    // Make the center the center of the track
    context.translate(-left, -top);
    carContext.translate(-left, -top);
    explosionContext.translate(-left, -top);

    // clear and apply background
    context.save();
    context.rect(left, top, width, height);
    context.fillStyle = backgroundPattern;
    context.fill();
    context.restore();
}

function getPieceXWithOffset(piece, offset) {
    var tangent = piece.startAngle + CANVAS_ANGLE_OFFSET;
    return piece.startPosition.x + Math.cos(toRadians(tangent)) * offset;
}

function getPieceYWithOffset(piece, offset) {
    var tangent = piece.startAngle + CANVAS_ANGLE_OFFSET;
    return piece.startPosition.y + Math.sin(toRadians(tangent)) * offset;
}

function getPieceEndXWithOffset(piece, offset) {
    var tangent = piece.endAngle + CANVAS_ANGLE_OFFSET;
    return piece.endPosition.x + Math.cos(toRadians(tangent)) * offset;
}

function getPieceEndYWithOffset(piece, offset) {
    var tangent = piece.endAngle + CANVAS_ANGLE_OFFSET;
    return piece.endPosition.y + Math.sin(toRadians(tangent)) * offset;
}
var piece_colors = ['blue', 'orange', 'white', 'yellow'];

function drawPiece(piece, offset, label) {
    var curX = getPieceXWithOffset(piece, offset);
    var curY = getPieceYWithOffset(piece, offset);

    if (!piece.length) {
        // Are we turning left or right?
        var counterClockwise = piece.angle < 0;

        // Adjust the offset depending how we are drawing
        var adjstedOffset = counterClockwise ? offset : -offset;

        // Now find the center
        var tangent = piece.startAngle + (counterClockwise ? -CANVAS_ANGLE_OFFSET : CANVAS_ANGLE_OFFSET);
        var centerX = curX + Math.cos(toRadians(tangent)) * (piece.radius + adjstedOffset);
        var centerY = curY + Math.sin(toRadians(tangent)) * (piece.radius + adjstedOffset);
        piece.centerX = centerX;
        piece.centerY = centerY;

        // Find the angle from center to the current point
        var curPointAngle = toRadians(tangent - 180);
        var endPointAngle = toRadians(piece.startAngle + piece.angle + (counterClockwise ? -270 : -90));

        // Draw the curve
        context.moveTo(curX, curY);
        context.arc(centerX, centerY, piece.radius + adjstedOffset, curPointAngle, endPointAngle, counterClockwise);
        context.moveTo(endX, endY);
    } else {
        // we are at a straight away
        var endX = getPieceEndXWithOffset(piece, offset);
        var endY = getPieceEndYWithOffset(piece, offset);

        // Draw the line
        context.moveTo(curX, curY);
        context.lineTo(endX, endY);
    }

}

function labelPiece(piece, offset, label) {
    var curX = getPieceXWithOffset(piece, offset);
    var endX = getPieceEndXWithOffset(piece, offset);
    var textX = (curX < endX) ? curX - (curX - endX) / 4 : curX + Math.abs(curX - endX) / 4;
    var textY = piece.length ? piece.endPosition.y : piece.midY;

    context.font = "32px Georgia red";
    context.fillText(label, textX, textY);
}

var lastCars = null;

function drawCars() {
    var jsontext = $("#carJson")[0].value;
    var cars = JSON.parse(jsontext);
    lastCars = cars;
    for (var i = 0; i < cars.length; i++) {
        drawCar(cars[i]);
    }
}

function drawCar(car) {
    carContext.save();
    carContext.fillStyle = car.id.color;

    var piece = _track.pieces[car.piecePosition.pieceIndex];

    var anchor = racecars[car.id.color].anchor;
    var length = racecars[car.id.color].length;
    var width = racecars[car.id.color].width;

    // We need to figure out our lane offset!
    var startOffset = _track.lanes[car.piecePosition.lane.startLaneIndex].distanceFromCenter;
    var endOffset = _track.lanes[car.piecePosition.lane.endLaneIndex].distanceFromCenter;

    var ratio;

    if (!piece.length) {
        // For lane switching on a curve, just draw in between lanes
        ratio = 0.5;
    } else {
        ratio = (piece.length - car.piecePosition.inPieceDistance) / piece.length;
    }

    var offset = startOffset * ratio + endOffset * (1 - ratio);

    var x;
    var y;
    var carAngle;

    if (!piece.length) {
        // We are at a bend
        // Are we turning left or right?
        var counterClockwise = piece.angle < 0;

        // Adjust the offset depending how we are drawing
        var adjstedOffset = counterClockwise ? offset : -offset;
        var radiansTravelled = car.piecePosition.inPieceDistance / (piece.radius + adjstedOffset);
        var angle = counterClockwise ? toRadians(piece.startAngle + CANVAS_ANGLE_OFFSET) - radiansTravelled : toRadians(piece.startAngle - CANVAS_ANGLE_OFFSET) + radiansTravelled;

        x = piece.centerX + Math.cos(angle) * (piece.radius + adjstedOffset);
        y = piece.centerY + Math.sin(angle) * (piece.radius + adjstedOffset);

        carAngle = toRadians(car.angle) + (counterClockwise ? angle - Math.PI / 2 : angle + Math.PI / 2);
    } else {
        // We are at a straight away, add our in piece distance
        carAngle = toRadians(piece.startAngle + car.angle);
        x = getPieceXWithOffset(piece, offset) + Math.cos(toRadians(piece.startAngle)) * car.piecePosition.inPieceDistance;
        y = getPieceYWithOffset(piece, offset) + Math.sin(toRadians(piece.startAngle)) * car.piecePosition.inPieceDistance;
    }

    // Now we have the car's anchor position and rotation, so let's get the corners
    // http://stackoverflow.com/questions/644378/drawing-a-rotated-rectangle
    var rect = [];
    rect.ul = [];
    rect.ul.x = x + ((length - anchor) / 2) * Math.cos(carAngle) - (width / 2) * Math.sin(carAngle);
    rect.ul.y = y + (width / 2) * Math.cos(carAngle) + ((length - anchor) / 2) * Math.sin(carAngle);

    rect.ur = [];
    rect.ur.x = x - ((length + anchor) / 2) * Math.cos(carAngle) - (width / 2) * Math.sin(carAngle);
    rect.ur.y = y + (width / 2) * Math.cos(carAngle) - ((length + anchor) / 2) * Math.sin(carAngle);

    rect.bl = [];
    rect.bl.x = x + ((length - anchor) / 2) * Math.cos(carAngle) + (width / 2) * Math.sin(carAngle);
    rect.bl.y = y - (width / 2) * Math.cos(carAngle) + ((length - anchor) / 2) * Math.sin(carAngle);

    rect.br = [];
    rect.br.x = x - ((length + anchor) / 2) * Math.cos(carAngle) + (width / 2) * Math.sin(carAngle);
    rect.br.y = y - (width / 2) * Math.cos(carAngle) - ((length + anchor) / 2) * Math.sin(carAngle);

    // Draw our rectangle!
    carContext.beginPath();
    carContext.moveTo(rect.ul.x, rect.ul.y);
    carContext.lineTo(rect.ur.x, rect.ur.y);
    carContext.lineTo(rect.br.x, rect.br.y);
    carContext.lineTo(rect.bl.x, rect.bl.y);
    carContext.fill();

    // Debugging, draw an anchor rect
    carContext.fillStyle = "green";
    carContext.fillRect((x) - 1, (y) - 1, 2, 2);

    carContext.restore();
}

function drawSwitch(piece, position, otherPosition) {
    var startX1 = getPieceXWithOffset(piece, position);
    var startY1 = getPieceYWithOffset(piece, position);
    var startX2 = getPieceXWithOffset(piece, otherPosition);
    var startY2 = getPieceYWithOffset(piece, otherPosition);
    var endX1 = getPieceEndXWithOffset(piece, position);
    var endY1 = getPieceEndYWithOffset(piece, position);
    var endX2 = getPieceEndXWithOffset(piece, otherPosition);
    var endY2 = getPieceEndYWithOffset(piece, otherPosition);

    context.moveTo(startX1, startY1);
    context.lineTo(endX2, endY2);
    context.moveTo(startX2, startY2);
    context.lineTo(endX1, endY1);
}

function drawStartLine(start) {
    // Make the start line 60 long,
    var lineRadius = 30;

    // Calculate end position
    var tangent = start.angle;
    var startX = start.position.x + Math.cos(toRadians(tangent)) * lineRadius;
    var startY = start.position.y + Math.sin(toRadians(tangent)) * lineRadius;
    var endX = start.position.x + Math.cos(toRadians(tangent - 180)) * lineRadius;
    var endY = start.position.y + Math.sin(toRadians(tangent - 180)) * lineRadius;

    // Now do the drawing
    context.save();
    context.beginPath();
    context.lineWidth = 5;
    context.strokeStyle = "yellow";
    context.moveTo(startX, startY);
    context.lineTo(endX, endY);
    context.stroke();
    context.restore()
}

function drawTitle(name) {
    context.font = "32px Georgia";
    context.fillText(name, trackLeft, trackTop - CANVAS_PADDING / 2 + 30);
}

function saveCarInfo(cars) {
    // We don't want to hard code the anchor point or the size of the car
    racecars = [];
    var carInfo;
    for (carInfo in cars) {
        racecars[cars[carInfo].id.color] = [];
        racecars[cars[carInfo].id.color].anchor = cars[carInfo].dimensions.guideFlagPosition;
        racecars[cars[carInfo].id.color].length = cars[carInfo].dimensions.length;
        racecars[cars[carInfo].id.color].width = cars[carInfo].dimensions.width;
    }
}

function onGameInit(data) {
    draw(data.race.track);
    saveCarInfo(data.race.cars)
}

function onCarPositions(data) {
    carContext.clearRect(trackLeft - CANVAS_PADDING / 2, trackTop - CANVAS_PADDING / 2, carCanvas.width, carCanvas.height);
    for (var i = 0; i < data.length; i++) {
        drawCar(data[i]);
    }
}

function rand(lo, hi) {
    return lo + Math.random() * (hi - lo);
}

function randColor() {
    return '#' + Math.floor(Math.random() * 16777215).toString(16);
}

function ParticleEffect() {
    this.x = 0;
    this.y = 0;
    this.age = 0;
    this.parts = [];
    this.rate = 1;
    this.max = 10;
    this.maxage = 200;
    this.color = "#aaaaaa";
    this.alive = true;
    this.initParticle = function(part) {
        return part;
    }
    this.tick = function() {
        this.age++;
        if (this.age > this.maxage) {
            this.alive = false;
            return;
        }
        if ((this.age % this.rate) == 0 && this.parts.length < this.max) {
            var part = {
                x: this.x,
                y: this.y,
                dx: rand(-3, 3),
                dy: rand(-3, 3),
                alpha: 1,
                dalpha: -0.01,
                color: this.color,
            };
            this.parts.push(this.initParticle(part));
        }
        for (var i = 0; i < this.parts.length; i++) {
            var p = this.parts[i];
            p.y += p.dy;
            p.x += p.dx;
            p.alpha += p.dalpha;
        }
    }
    this.draw = function(g) {
        if (!this.alive) return;
        g.save();
        for (var i = 0; i < this.parts.length; i++) {
            var p = this.parts[i];
            if (p.alpha < 0) continue;
            g.fillStyle = p.color;
            g.globalAlpha = p.alpha;
            g.fillRect(p.x, p.y, 20, 20);
        }
        g.restore();
    }
}

function onCrash(data) {
    if (!lastCars) {
        return;
    }
    var crashEvent = data;
    var car = null;
    for (var i = 0; i < lastCars.length; i++) {
        if (lastCars[i].id.name === crashEvent.name) {
            car = lastCars[i];
            break;
        }
    }
    if (!car) {
        car = lastCars[0];
    }


    // calculate cars anchor position
    var piece = _track.pieces[car.piecePosition.pieceIndex];
    var offset = _track.lanes[car.piecePosition.lane.startLaneIndex].distanceFromCenter;

    var pos = {};

    if (!piece.length) {
        // We are at a bend
        // Are we turning left or right?
        var counterClockwise = piece.angle < 0;

        // Adjust the offset depending how we are drawing
        var adjstedOffset = counterClockwise ? offset : -offset;
        var radiansTravelled = car.piecePosition.inPieceDistance / (piece.radius + adjstedOffset);
        var angle = counterClockwise ? toRadians(piece.startAngle + CANVAS_ANGLE_OFFSET) - radiansTravelled : toRadians(piece.startAngle - CANVAS_ANGLE_OFFSET) + radiansTravelled;

        pos.x = piece.centerX + Math.cos(angle) * (piece.radius + adjstedOffset);
        pos.y = piece.centerY + Math.sin(angle) * (piece.radius + adjstedOffset);

    } else {
        // We are at a straight away, add our in piece distance
        pos.x = getPieceXWithOffset(piece, offset) + Math.cos(toRadians(piece.startAngle)) * car.piecePosition.inPieceDistance;
        pos.y = getPieceYWithOffset(piece, offset) + Math.sin(toRadians(piece.startAngle)) * car.piecePosition.inPieceDistance;
    }

    var effect = new ParticleEffect();
    effect.x = pos.x;
    effect.y = pos.y;
    var start = new Date().getTime();
    var f = function() {
        explosionContext.save();
        effect.tick();
        effect.draw(explosionContext);
        explosionContext.restore();
        if (new Date().getTime() - start >= 3000) {
            explosionContext.clearRect(trackLeft - CANVAS_PADDING / 2, trackTop - CANVAS_PADDING / 2, explosionCanvas.width, explosionCanvas.height);
        } else {
            setTimeout(f, 1000.0 / 60.0);
        }
    };

    f();
}

$(function() {
    var message_handlers = {};
    message_handlers["gameInit"] = onGameInit;
    message_handlers["carPositions"] = onCarPositions;
    message_handlers["crash"] = onCrash;
    message_handlers["metrics"] = Telemetry.update;

    //start the listening
    var connection = new WebSocket('ws://localhost:8080/events');
    connection.onopen = function() {
        console.log('connected');
        connection.send('ping');
    };

    connection.onmessage = function(e) {
        var payload;
        try {
            payload = JSON.parse(e.data);
        } catch (e) {
            console.log('Server: ' + e);
            return;
        }
        if (message_handlers[payload.msgType]) {
            message_handlers[payload.msgType](payload.data);
        } else {
            console.log('Server: ' + e.data);
        }
    }
    setTimeout(function() {
        drawTrack();

        racecars["red"] = [];
        racecars["red"].anchor = 10;
        racecars["red"].length = 40;
        racecars["red"].width = 20;

        drawCars();
    }, 1000);
});