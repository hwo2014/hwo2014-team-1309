window.Telemetry = (function() {

    var metricsHistory = {};

    var contexts = {};

    function drawThrottle(t, offset) {
        drawValue(contexts['throttle'], t, 0.0, 1.0, offset, '#013A5B');
    }

    function drawAngle(a, offset) {
        drawValue(contexts['angle'], a, -60.0, 60.0, offset, '#F02A46');
    }

    function drawAngleV(a, offset) {
        drawValue(contexts['anglev'], a, -5.0, 5.0, offset, '#ED912A');
    }

    function shiftGraph(ctx, offset) {
        var image = ctx.getImageData(offset, 0, ctx.canvas.width, ctx.canvas.height);
        ctx.beginPath()
        ctx.moveTo(ctx.canvas.width - offset + 0.5, ctx.canvas.height + 0.5);
        ctx.lineTo(ctx.canvas.width - offset + 0.5, 0 + 0.5);
        ctx.strokeStyle = '#FFFFFF';
        ctx.strokeWidth = offset;
        ctx.stroke();

        ctx.putImageData(image, 0, 0);
    }

    function drawValue(ctx, v, minV, maxV, offset, color) {
        height = ctx.canvas.height;
        range = height / ((maxV - minV) * 1.0);

        y0 = (0 - minV) * range;
        y1 = (v - minV) * range;

        if (y0 == y1) {
            y0 -= 0.5;
            y1 += 0.5;
        }

        ctx.beginPath();
        ctx.moveTo(offset + 0.5, height - y0);
        ctx.lineTo(offset + 0.5, height - y1);
        ctx.strokeStyle = color;
        ctx.strokeWidth = 1;
        ctx.stroke();

    }

    var lastTick = -1;

    return {
        init: function init() {
            contexts['throttle'] = document.getElementById("throttle").getContext('2d');
            contexts['angle'] = document.getElementById("angle").getContext('2d');
            contexts['anglev'] = document.getElementById("anglev").getContext('2d');

            for (var i in contexts) {
                contexts[i].canvas.width = 400;
                contexts[i].canvas.height = 100;
            }
        },
        update: function updateMetrics(data) {
            var tick = data['tick'];
            if (tick == -1) {
                return;
            }

            metricsHistory[tick] = data['metrics'];

            if (tick - lastTick > 10) {
                contexts['throttle'].clearRect(0, 0, contexts['throttle'].canvas.width, contexts['throttle'].canvas.height);
                for (var i = 0; i > -contexts['throttle'].canvas.width; i--) {
                    if (metricsHistory[tick + i]) {
                        drawThrottle(metricsHistory[tick + i]['throttle'], contexts['throttle'].canvas.width - 1 + i);
                    }
                }
            } else {

                var tO = tick - lastTick;

                shiftGraph(contexts['throttle'], tO)
                drawThrottle(metricsHistory[tick]['throttle'], contexts['throttle'].canvas.width - 1);

                shiftGraph(contexts['angle'], tO)
                drawAngle(metricsHistory[tick]['angle'], contexts['angle'].canvas.width - 1);

                shiftGraph(contexts['anglev'], tO)
                drawAngleV(metricsHistory[tick]['anglev'], contexts['anglev'].canvas.width - 1);


            }
            lastTick = tick
        }
    }
})();