import math
import numpy as np
import matplotlib.pyplot as plt
import random

arData = [[float(y) for y in x.split(',')] for x in open("slip_dynamics.csv").readlines()]

arData = [x for x in arData]

#1635,4,-0.125797,0.125797,5.757421,90.000000


#0.1105 = (90.0,5.713)
#0.0868 (110,6.198)
#0.2880 = (90,6.198)


print len(arData)

def findClusterCenters(data):
    clusters = []
    while (len(data) > 0):
        t = random.randint(0,len(data) - 1)

        center = data[t][4]

        cX = [x[4] for x in data if math.fabs(x[4] - center) < 0.01]

        center = np.mean(cX)

        cX = [x[4] for x in data if math.fabs(x[4] - center) < 0.01]

        if (len(cX) > 5):
            clusters.append((np.mean(cX), len(cX)))

        data = [x for x in data if math.fabs(x[4] - center) >= 0.01]

    return clusters

clusters = [x[0] for x in findClusterCenters(arData)]

radii = set([x[5] for x in arData if x[5] != 0])

dp = {}


for r in radii:
    vals = [(x[4], math.fabs(x[2])) for x in arData if x[5] == r]
    if len(vals) > 10:
        vals_s = sorted(vals, key=lambda tup: tup[0])

        dp[r] = (np.array([x[0]**2 for x in vals_s]), np.array([x[1] for x in vals_s]))



for points in dp:
    # get x and y vectors

    print(points)

    x = dp[points][0][1:-1]
    y = dp[points][1][1:-1]

    # calculate polynomial
    z = np.polyfit(x, y, 1)
    print(z)
    f = np.poly1d(z)

    # calculate new x's and y's
    x_new = np.linspace(x[0], x[-1], 50)
    y_new = f(x_new)

    plt.plot(x,y,'o', x_new, y_new)
    plt.xlim([x[0]-1, x[-1] + 1 ])
    plt.show()
