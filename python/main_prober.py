import json
import socket
import sys
import pprint
import Telemetry
import math
import Track
from Logging import *
crashPoints = set([3,5,13,14,15,19,27,32,33])
badCrashPoints = set([4,26])
gottaGoFast = set([18, 21, 20, 25, 28, 31])

class NoobBot(object):

    def __init__(self, socket, name, key, create=None, join=None, track=None, password=None, carcount=None, emit_host=None):
        self.socket = socket
        self.name = name
        self.key = key
        self.createMode = create
        self.joinMode = join
        self.trackName = track
        self.password = password

        if carcount != None:
            self.carcount = int(carcount)
        else:
            self.carcount = -1

        self.color = "red"

        self.emit_host = emit_host
        if emit_host:
            self.session = requests.Session()
            self.session.headers.update({'Content-Type':'application/json'})

        #Are we switching?
        self.pending_switch = False

        #The last piece ID for a curve on this map
        self.last_curve_piece = 0

        #The current lane piece our driver is on
        self.current_lane = 0

        #The last (current) piece our driver was on
        self.last_piece = 0

        #The last (current) lap our driver was on
        self.last_lap = 0

        #The total number of laps in this race
        self.total_laps = 0

        # Data about the track
        self.track_tracker = None

        # Flag to stop simulation
        self.dead = False

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def joinRace(self):
        return self.msg("joinRace", {"botId": {"name": self.name,
                                               "key": self.key},
                                       "trackName": self.trackName,
                                       "password": self.password,
                                       "carCount": self.carcount})


    def create(self):
        return self.msg("createRace", {"botId": {"name": self.name,
                                                 "key": self.key},
                                       "trackName": self.trackName,
                                       "password": self.password,
                                       "carCount": self.carcount})

    def throttle(self, throttle):
        self.car.on_throttle(throttle)
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        if self.createMode:
            self.create()
        elif self.joinMode:
            self.joinRace()
        else:
            self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        #pprint.PrettyPrinter().pprint(data)
        self.car.on_position(data, self.gameTick)

        #first update our data for the positions
        for car in data:
            if car['id'].get('name',"unknown") == self.name and car['id'].get('color',"unknown") == self.color:
                self.last_lap = car['piecePosition']['lap']
                self.last_piece = car['piecePosition']['pieceIndex']

                if self.current_lane != car['piecePosition']['lane']['endLaneIndex']:
                    self.current_lane = car['piecePosition']['lane']['endLaneIndex']
                    self.pending_switch = False

                break

        #Now figure our what our new throttle would be
        throttle = 0;
        if self.at_last_straightaway() or (self.last_piece == self.last_curve_piece and car.get('piecePosition', {}).get('inPieceDistance', 0) > 25) or (self.last_piece == 7 and car.get('piecePosition', {}).get('inPieceDistance', 0) > 20):
            throttle = 1.0
        elif self.last_piece in badCrashPoints or self.last_piece == 26:
            throttle = 0.2
        elif self.last_piece in gottaGoFast:
            throttle = 0.615
        elif (self.last_piece in crashPoints):
            throttle = 0.38
        else:
            throttle = 0.892

        # Now throttle if it is changing, otherwise send our switch lanes command
        if throttle != self.car.lastThrottle:
            self.throttle(throttle)
        elif self.last_piece == 7 and not self.pending_switch:
            self.msg('switchLane','Left')
            self.car.on_throttle(throttle)
            self.pending_switch = True
        elif self.last_piece == 17 and not self.pending_switch:
            self.car.on_throttle(throttle)
            self.msg('switchLane','Right')
            self.pending_switch = True
        elif self.last_piece == 1 and self.current_lane == 0 and not self.pending_switch:
            self.car.on_throttle(throttle)            
            self.msg('switchLane','Right')
            self.pending_switch = True
        else:
            self.throttle(throttle)

    def on_car_positions_ga(self, data):
        #pprint.PrettyPrinter().pprint(data)
        self.car.on_position(data, self.gameTick)

        #first update our data for the positions
        for car in data:
            if car['id'].get('name',"unknown") == self.name and car['id'].get('color',"unknown") == self.color:
                self.last_lap = car['piecePosition']['lap']
                self.last_piece = car['piecePosition']['pieceIndex']

                if self.current_lane != car['piecePosition']['lane']['endLaneIndex']:
                    self.current_lane = car['piecePosition']['lane']['endLaneIndex']
                    self.pending_switch = False

                break

        if self.last_piece == 1 and self.current_lane == 0 and not self.pending_switch:
            self.msg('switchLane','Right')
            self.pending_switch = True
        else:
            self.throttle(tps[int(self.car.track.track_position(self.car.lastPos) * 500)])

    def on_car_positions_fixed_throttle(self, data):
        #pprint.PrettyPrinter().pprint(data)
        self.car.on_position(data, self.gameTick)

        for car in data:
            if car['id'].get('name',"unknown") == self.name and car['id'].get('color',"unknown") == self.color:
                self.last_lap = car['piecePosition']['lap']
                self.last_piece = car['piecePosition']['pieceIndex']

                if self.current_lane != car['piecePosition']['lane']['endLaneIndex']:
                    self.current_lane = car['piecePosition']['lane']['endLaneIndex']
                    self.pending_switch = False

                break

        if self.last_piece == 7 and not self.pending_switch:
            self.msg('switchLane','Left')
            self.pending_switch = True
        elif self.last_piece == 17 and not self.pending_switch:
            self.msg('switchLane','Right')
            self.pending_switch = True
        elif self.last_piece == 1 and self.current_lane == 0 and not self.pending_switch:
            self.msg('switchLane','Right')
            self.pending_switch = True
        else:
            mst = self.car.simVersion.findMaxSafeThrottle()
            if car['piecePosition']['lane']['endLaneIndex'] != car['piecePosition']['lane']['startLaneIndex']:
                mst += 0.03
            if mst > 1.0:
                mst = 1.0
            self.throttle(mst)

    def on_crash(self, data):
        name = data.get('name', "unknown")
        color = data.get('color', "unknown")
        self.dead = True
        if name == self.name and color == self.color:
            print("We crashed at {0}".format(self.last_piece))
        else:
            print("{0} crashed".format(name))
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        pprint.PrettyPrinter().pprint(data)
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def on_your_car(self, data):
        self.name = data['name']
        self.color = data['color']
        print("Setting Car Name: {0}, Color: {1}".format(self.name, self.color))
        self.ping()

    def on_lap_finished(self, data):
        name = data.get('car', {}).get("name", "unknown")
        color = data.get('car', {}).get("color", "unknown")
        lap = data.get('lapTime', {}).get("lap", -1)
        lapTime = data.get('lapTime', {}).get("millis", "unknown")
        if self.name == name and self.color == color:
            print("We finished lap {0} in {1}".format(lap+1,lapTime))
        else:
            print("{0} car finished lap {1} in {2}".format(color,lap+1,lapTime))
        self.ping()

    def on_finish(self, data):
        name = data.get('name', "unknown")
        color = data.get('color', "unknown")
        if self.name == name and self.color == color:
            print("We finished!")
        else:
            print("{0} car finished...".format(color))

        self.ping()

    def on_game_init(self, data):
        logO(pprint.PrettyPrinter().pformat(data))
        self.track_tracker = Track.Track(data['race']['track'])
        
        self.run_compressor = Telemetry.InfoDumper(data['race']['cars'])

        for c in data['race']['cars']:
            if c['id']['color'] == self.color:
                self.car = Telemetry.CarTracker(0, self.track_tracker, c['dimensions'])
                break
        else:
            logA("Dude, where's our car?")

        self.total_laps = data['race']['raceSession'].get('laps', 3)

        self.ping()

    def msg_loop(self):
        msg_map = {
            'gameInit' : self.on_game_init,
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions_simple,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'yourCar': self.on_your_car,
            'finish': self.on_finish,
            'lapFinished': self.on_lap_finished,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line and not self.dead:
            if self.emit_host:
                try:
                    self.session.post('http://%s/receiver' % self.emit_host, data=line)
                except:
                    pass
            msg = json.loads(line)

            self.gameTick = msg.get('gameTick', -1)

            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0} at time {1}".format(msg_type, self.gameTick))
                pprint.PrettyPrinter().pprint(data)
                self.ping()
            line = socket_file.readline()

    def at_last_straightaway(self):
        #If we have passed the last curve, book it!
        if self.last_lap == self.total_laps - 1 and self.track_tracker.lastCurve < self.last_piece:
            return True
        else:
            return False

    def on_car_positions_simple(self, data):
        #pprint.PrettyPrinter().pprint(data)
        self.car.on_position(data, self.gameTick)

        #first update our data for the positions
        for car in data:
            if car['id']['name'] == name:
                self.last_lap = car['piecePosition']['lap']
                self.last_piece = car['piecePosition']['pieceIndex']

                if self.current_lane != car['piecePosition']['lane']['endLaneIndex']:
                    self.current_lane = car['piecePosition']['lane']['endLaneIndex']
                    self.pending_switch = False

        if self.last_piece == 1 and self.current_lane == 0 and not self.pending_switch and self.should_switch:
            self.msg('switchLane','Right')
            self.pending_switch = True
        elif self.car.lastPos['lap'] < 1:
            # Drive carefully up to the last bit
            if self.last_piece < 49:
                self.throttle(0.35)
            # Kill as much angular wobble as possible by creeping up
            elif self.last_piece < 55:
                self.throttle(0.05)
            elif self.car.velocity < self.target:
                self.throttle(1.0)
            else:
                self.throttle(self.target/10.0)
        elif self.last_piece > 6:
            self.dead = True
        elif self.car.velocity < self.target:
            self.throttle(1.0)
        else:
            self.throttle(self.target/10.0)


if __name__ == "__main__":
    print (len(sys.argv))
    if len(sys.argv) < 5:
        print("Usage: ./run host port botname botkey")
        print("Usage: ./run host port botname botkey serverhost")
        print("Usage: ./run host port botname botkey create|join track password carcount")
        print("Usage: ./run host port botname botkey create|join track password carcount serverhost")
    else:
        host, port, name, key = sys.argv[1:5]
        emit_host=None
        track = None
        password = None
        carcount = None
        create = False
        join = False
        if len(sys.argv) == 10 or len(sys.argv) == 6:
            import requests
            emit_host = sys.argv[len(sys.argv) - 1]
            print("emitting to " + emit_host)
        if len(sys.argv) >= 9:
            track = sys.argv[6]
            password = sys.argv[7]
            carcount = sys.argv[8]
            if sys.argv[5] == 'create':
                create = True
            else:
                join = True
            print("mode={0}, track={1}, password={2}, carcount={3}".format(*sys.argv[5:9]))
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))

        for sT in range(16,36):

            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((host, int(port)))
            bot = NoobBot(s, name, key, create=create, join=join, track=track, password=password, carcount=carcount, emit_host=emit_host)
            bot.target = sT / 4.0
            bot.should_switch = False
            bot.run()

            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((host, int(port)))
            bot = NoobBot(s, name, key, create=create, join=join, track=track, password=password, carcount=carcount, emit_host=emit_host)
            bot.target = sT / 4.0
            bot.should_switch = True
            bot.run()


        open("telemetry_data.json","w").write(json.dumps(Telemetry.CarTracker.history))

        """for sT in range(8, 36):
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((host, int(port)))
            bot = NoobBot(s, name, key, emit_host=emit_host)
            bot.target = sT / 4.0
            bot.should_switch = True
            bot.run()

        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key, emit_host=emit_host)
        bot.target = 6.0
        bot.should_switch = True
        bot.run()"""
