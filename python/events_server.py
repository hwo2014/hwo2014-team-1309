__author__ = 'seanfitz'
from bottle import default_app, post, request, get
import json
from pymongo import MongoClient
client = MongoClient()
event_collection = client['helloworld']['events']

@post("/events")
def receive_events():
    payload = json.loads(request.body.read())
    for event in payload.get('events'):
        event_collection.save({
            'gameId':payload.get('gameId'),
            'event':json.loads(event)
        })

@get("/games")
def list_games():
    return json.dumps(event_collection.distinct('gameId'))

@get("/games/<game_id>")
def get_game(game_id):
    events = event_collection.find({'gameId': game_id}).sort('event.tick')
    result = []
    for event in events:
        event.pop('_id')
        result.append(event)
    return json.dumps(result)

application = default_app()

application.run(host='0.0.0.0', port=8000)