import json
import socket
import sys
import pprint
import Telemetry
import Track
from Logging import *


def processRun(runData):
	gameInit = [x for x in runData if x['msgType'] == 'gameInit'][0]['data']
	trackData = gameInit['race']['track']
	track = Track.Track(trackData)

	color = [x for x in runData if x['msgType'] == 'yourCar'][0]['data']['color']

	rCP = [x for x in runData if x['msgType'] == 'carPositions'][10]['data']

	whichCar = -1

	for i in range(0, len(rCP)):
		if rCP[i]['id']['color'] == color:
			whichCar = i

	car = Telemetry.CarTracker(color, track)

	for d in runData:
		if d['msgType'] == 'carPositions':
			tick = d.get('gameTick', -1)
			car.on_position(d['data'], tick)
		if d['msgType'] == 'throttle':
			car.on_throttle(d['data'])


if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "Usage: RunPlayer <run_json>"
		sys.exit(0)

	rData = json.load(open(sys.argv[1]))

	processRun(rData)

