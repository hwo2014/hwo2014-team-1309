import math
from scipy.optimize import minimize
import numpy as np

lines = [x.strip() for x in open("centripetal.csv").readlines()][1:]

data = [[float(y) for y in x.split(",")] for x in lines]

#0-Tick
#1-Ang
#2-AngV
#3-V
#4-R

sK = -0.00745345
dK =  -0.0985848
"""
            p0 pos += vel v0
            p1 = p0 + v0
            #print("%f,%f"%(pos-targets[x],pos))

            erracc += (pos-targets[x]) ** 2


            fS = (0 - pos) * sK

            v1 = v0 + -p1 * sK + v0 * dK

            vel += fS + fK * vel * -1.0
"""



dumpa = open("cent_out.csv", "w")
dumpa.write("Tick,Ang,AngV,V,Radius,CurveDirections,PieceId,Miss\n")
def computeError(x):
    totalError = 0
    for i in range(1, len(data) - 1):
        if data[i][4] == -1.0 and data[i - 1][4] == -1.0 and data[i + 1][4] == -1.0 and data[i - 2][4] == -1.0:
            #Angle''(t) = CentrifugalForceOrSomething + Angle'(t-1) * (Constant1 + Constant2 * Velocity) + Angle(t-2) * (Constant2 * Velocity)
            #Angle''(t) = data[i][2] - data[i - 1][2]
            #Angle'(t - 1) = data[i - 1][2]
            #Angle(t - 2) = data[i - 2][1]

            aDt = data[i][2] - data[i - 1][2]
            aSt = data[i - 1][2]

            totalError += (aDt - ((aSt * (x[0] + x[1] * data[i - 1][3])) + data[i - 1][1] * (x[1] * data[i - 1][3]))) ** 2

            #nA = data[i][2] + data[i - 2][1] * x[0] + data[i - 1][2] * x[1]
            #totalError += (data[i + 1][2] - nA) ** 2

    return math.sqrt(totalError)

def printError(x):
    totalError = 0
    for i in range(1, len(data) - 1):
        #if data[i][4] == -1.0 and data[i - 1][4] == -1.0 and data[i + 1][4] == -1.0 and data[i - 2][4] == -1.0:
            #Angle''(t) = CentrifugalForceOrSomething + Angle'(t-1) * (Constant1 + Constant2 * Velocity) + Angle(t-2) * (Constant2 * Velocity)
            #Angle''(t) = data[i][2] - data[i - 1][2]
            #Angle'(t - 1) = data[i - 1][2]
            #Angle(t - 2) = data[i - 2][1]

        aDt = data[i][2] - data[i - 1][2]
        aSt = data[i - 1][2]

        print aDt - ((aSt * (sK + dK * data[i - 1][3])) + data[i - 2][1] * (dK * data[i - 1][3]))

        dumpa.write("%d,%f,%f,%f,%f,%f,%d,%f\n" % (data[i][0], data[i][1], data[i][2], data[i][3], data[i][4], data[i][5], data[i][6], 
            aDt - ((aSt * (sK + dK * data[i - 1][3])) + data[i - 2][1] * (dK * data[i - 1][3]))))

            #totalError += (aDt - (aSt * (x[0] + x[1] * data[i - 1][3])) + data[i - 1][1] * (x[1] * data[i - 1][3])) ** 2

            #nA = data[i][2] + data[i - 2][1] * x[0] + data[i - 1][2] * x[1]
            #totalError += (data[i + 1][2] - nA) ** 2



def computeAngles(x):
    totalError = 0
    for i in range(1, len(data) - 1):
        if data[i][4] != -1.0 and data[i - 1][4] != -1.0 and data[i + 1][4] != -1.0 and data[i - 2][4] != -1.0 and data[i - 1][0] < 1000:
            aDt = data[i][2] - data[i - 1][2]
            aSt = data[i - 1][2]

            cf = max(x[0] * ((data[i - 1][3] ** 2)/data[i - 1][4]) + x[1], 0) * data[i - 1][5]        
            rf = ((aSt * (sK + dK * data[i - 1][3])) + data[i - 2][1] * (dK * data[i - 1][3]))
            #cf = max(x[0] * ((data[i - 1][3] ** 2)/data[i - 1][4]) + x[1], 0) * data[i - 1][5]

            #dumpa.write("%d,%f,%f,%f,%f,%f,%d,%f\n" % (data[i][0], data[i][1], data[i][2], data[i][3], data[i][4], data[i][5], data[i][6], aDt - rf))
            #print aDt - (rf + cf)

                #print (aDt - rf)
            totalError += (aDt - (rf + cf)) ** 2

    return math.sqrt(totalError)

print(computeError([sK, dK]))

x0 = np.array([sK, dK])
res = minimize(computeError, x0, method='nelder-mead',
                options={'xtol': 1e-8, 'disp': True})

print(res)
sK = res.x[0]
dK = res.x[1]

printError(res.x)




#computeAngles(x0)
x0 = np.array([2.75, -1.0])
res = minimize(computeAngles, x0, method='nelder-mead',
                options={'xtol': 1e-8, 'disp': True})

print(res)