import tornado.ioloop
import tornado.web
import tornado.websocket
import os
import uuid

active_client_callbacks = {}


class EventReceiver(tornado.web.RequestHandler):
    def post(self):
        for client in active_client_callbacks:
            active_client_callbacks[client](self.request.body)


class MyWebSocketHandler(tornado.websocket.WebSocketHandler):
    def open(self):
        print("new client connected")
        self.id = str(uuid.uuid1())
        active_client_callbacks[self.id] = self.write_message
        self.write_message("You are connected")

    def on_message(self, message):
        print(message)

    def on_close(self):
        del active_client_callbacks[self.id]


settings = {
    'debug': True
}

application = tornado.web.Application([
    (r"/receiver", EventReceiver),
    (r"/events", MyWebSocketHandler),
    (r"/(.*)", tornado.web.StaticFileHandler, {"path": os.path.dirname(os.path.realpath(__file__)) + "/static"}),
], **settings)

if __name__ == "__main__":
    print(os.path.dirname(os.path.realpath(__file__)) + "/static")
    application.listen(8080)
    tornado.ioloop.IOLoop.instance().start()


