import getpass
import os

running_user = ''

if os.name == 'nt':
    running_user = 'cody'
else:
    cur_user = getpass.getuser()

    if cur_user == 'charles':
        running_user = 'charles'
    elif cur_user == 'seanfitz':
        running_user = 'sean'

def log(user, msg):
    global running_user
    if user == 'all' or user == running_user:
        print(msg)
    elif user == 'other':
        if '' == running_user or "HWO_SHOW_OTHER_LOG" in os.environ:
            print(msg)

def logA(msg):
    log("all", msg)

def logCV(msg):
    log("charles", msg)

def logCM(msg):
    log("cody", msg)

def logSF(msg):
    log("sean", msg)

def logO(msg):
    log("other", msg)