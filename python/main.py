import os
import sys

if os.name == 'nt':
    sys.path = [
 'C:\\Anaconda\\python27.zip',
 'C:\\Anaconda\\DLLs',
 'C:\\Anaconda\\lib',
 'C:\\Anaconda\\lib\\plat-win',
 'C:\\Anaconda\\lib\\lib-tk',
 'C:\\Anaconda',
 'C:\\Anaconda\\lib\\site-packages',
 'C:\\Anaconda\\lib\\site-packages\\PIL',
 'C:\\Anaconda\\lib\\site-packages\\win32',
 'C:\\Anaconda\\lib\\site-packages\\win32\\lib',
 'C:\\Anaconda\\lib\\site-packages\\Pythonwin',
 'C:\\Anaconda\\lib\\site-packages\\setuptools-2.2-py2.7.egg',
 'C:\\Anaconda\\lib\\site-packages\\IPython\\extensions',
 '']

import json
import socket
import sys
import pprint
import Telemetry
from Simulator import WORLDPHYSICS
import Track
from Logging import *
import time
from multiprocessing import Pipe, Process

class NoobBot(object):

    def __init__(self, socket, name, key, create=None, join=None, track=None, password=None, carcount=None, emit_host=None):
        self.socket = socket
        self.name = name
        self.key = key
        self.createMode = create
        self.joinMode = join
        self.trackName = track
        self.password = password
        self.logging_buffer = []
        self.message_history = []

        self.stopRunning = False

        socket_file = s.makefile()
        self.remote_conn, child_conn = Pipe()
        p = Process(target=self.off_process_msg_loop, args=(child_conn,self.socket, socket_file))
        p.start()

        if carcount != None:
            self.carcount = int(carcount)
        else:
            self.carcount = -1

        self.color = "red"

        self.emit_host = emit_host
        if emit_host:
            self.session = requests.Session()
            self.session.headers.update({'Content-Type':'application/json'})

        #Are we switching?
        self.pending_switch = False

        #The last piece ID for a curve on this map
        self.last_curve_piece = 0

        #The total number of laps in this race
        self.total_laps = 0

        # Data about the track
        self.track_tracker = None

        self.gameId = None

    def msg(self, msg_type, data):
        self.lastThrottle = 0
        self.remote_conn.send(("server", json.dumps({"msgType": msg_type, "data": data})))     


    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def joinRace(self):
        return self.msg("joinRace", {"botId": {"name": self.name,
                                               "key": self.key},
                                       "trackName": self.trackName,
                                       "password": self.password,
                                       "carCount": self.carcount})


    def create(self):
        return self.msg("createRace", {"botId": {"name": self.name,
                                                 "key": self.key},
                                       "trackName": self.trackName,
                                       "password": self.password,
                                       "carCount": self.carcount})

    def throttle(self, throttle):
        self.car.on_throttle(throttle)
        self.msg("throttle", throttle)
        if emit_host:
            self.session.post('http://%s/receiver' % self.emit_host, data=json.dumps({
                "msgType":"metrics",
                "data":
                    {
                    "tick": self.gameTick,
                    "metrics": {
                        "throttle": throttle,
                        "angle": self.car.lastAngle,
                        "anglev": self.car.angularVelocity,
                        "speed": self.car.velocity
                    }
                }}), timeout=.1)


    def send_turbo(self):
        self.msg("turbo", "PEW PEW PEW")
        self.car.carList[self.color].hasTurbo = False
        self.car.on_car_changed()

    def ping(self):
        self.msg("ping", {})

    def run(self):
        if self.createMode:
            self.create()
        elif self.joinMode:
            self.joinRace()
        else:
            self.join()
        self.msg_loop()

    def on_join(self, data):
        logA("Joined")
        self.ping()

    def on_game_start(self, data):
        logA("Race started")
        self.ping()

    def on_turbo_available(self, data):
        logCV(("Turbo available", json.dumps(data)))

        for color in self.car.carList:
            if not self.car.carList[color].isCrashed:
                self.car.carList[color].hasTurbo = data

                if not self.car.carList[color].inTurbo:
                    self.car.carList[color].turboMultiplier = data['turboFactor']
                    self.car.carList[color].turboDuration = data['turboDurationTicks']

        self.car.on_car_changed()

    def on_turbo_start(self, data):
        name = data.get('name', "unknown")
        color = data.get('color', "unknown")
        if name == self.name and color == self.color:
            logA("We used turbo at {0}".format(self.car.carList[self.color].pieceIndex))
        else:
            logA("{0} used turbo".format(color))
        self.car.carList[color].hasTurbo = False
        self.car.carList[color].inTurbo = True
        self.car.carList[color].turboStartTick = self.gameTick
        self.car.on_car_changed()
        self.ping()

    def on_turbo_end(self, data):
        name = data.get('name', "unknown")
        color = data.get('color', "unknown")
        if name == self.name and color == self.color:
            logA("Our turbo ended at {0}".format(self.car.carList[self.color].pieceIndex))
        else:
            logA("{0} is done with turbo".format(color))
        self.car.carList[color].inTurbo = False
        if self.car.carList[color].hasTurbo:
            self.car.carList[color].turboMultiplier = self.car.carList[color].hasTurbo['turboFactor']
            self.car.carList[color].turboDuration = self.car.carList[color].hasTurbo['turboDurationTicks']
        self.car.on_car_changed()
        self.ping()


    def on_car_positions_bump_nerdz(self, data):
        self.car.on_position(data, self.gameTick)
        try:
            onASwitch = False
            if self.car.carList[self.color].pieceIndex in self.track_tracker.switches:
                #We are on a switch, reset our pending switch
                self.pending_switch = False
                self.car.simVersion.pendingSwitch = None
                onASwitch = True

            nextSwitch = self.car.simVersion.nextSwitchDirection(self.car.carList[self.color].endLane)
            if nextSwitch and not self.pending_switch and not onASwitch and self.gameTick > 5:
                self.msg('switchLane', nextSwitch)
                self.pending_switch = True
                switchInfo = self.car.simVersion.nextSwitchLaneAndPiece(self.car.carList[self.color].endLane)
                self.car.simVersion.pendingSwitch = (switchInfo[1], self.car.carList[self.color].endLane, switchInfo[0])

            elif self.car.carList[self.color].hasTurbo and (self.at_last_straightaway(diff=0) or self.car.carList[self.color].pieceIndex == self.track_tracker.longestStraightStart) and WORLDPHYSICS.goForIt:
                self.send_turbo()
            elif (len(self.car.carList[self.color].inFrontOfUs) > 0 
                and not 'angle' in self.track_tracker.trackData['pieces'][self.car.carList[self.color].pieceIndex] 
                and not (self.car.carList[self.color].justBumped 
                and 'angle' in self.track_tracker.trackData['pieces'][self.car.carList[self.car.carList[self.color].inFrontOfUs[0]].pieceIndex])):
                logCV("ATTACK!!!")
                self.throttle(min(self.car.simVersion.findMaxSafeThrottle() + 0.2, 1.0))
            elif (len(self.car.carList[self.color].inFrontOfUs) > 0 
                and not 'angle' in self.track_tracker.trackData['pieces'][self.car.carList[self.color].pieceIndex] 
                and not (self.car.carList[self.color].justBumped 
                and not 'angle' in self.track_tracker.trackData['pieces'][self.car.carList[self.car.carList[self.color].inFrontOfUs[0]].pieceIndex]
                and 'angle' in self.track_tracker.trackData['pieces'][self.car.carList[self.car.carList[self.color].inFrontOfUs[0]].pieceIndex + 1 % len(self.track_tracker.trackData['pieces'])])):
                logCV("ATTACK!!!")
                self.throttle(min(self.car.simVersion.findMaxSafeThrottle() + 0.5, 1.0))

            else:
                mst = self.car.simVersion.findMaxSafeThrottle()

                if mst > 1.0:
                    mst = 1.0

                if self.at_last_straightaway(diff=0):
                    mst = 1.0
                self.throttle(mst)
        except:
            self.ping()
            pass


    def on_car_positions_fixed_throttle(self, data):
        if (self.color == "blue" or self.color == "yellow"):
            self.on_car_positions_bump_nerdz(data)
            return
        millis = int(round(time.time() * 1000))

        self.car.on_position(data, self.gameTick)
        self.run_compressor.on_car_positions(data, self.gameTick)
        
        onASwitch = False
        if self.car.carList[self.color].pieceIndex in self.track_tracker.switches:
            #We are on a switch, reset our pending switch
            self.pending_switch = False
            self.car.simVersion.pendingSwitch = None
            onASwitch = True

        nextSwitch = self.car.simVersion.nextSwitchDirection(self.car.carList[self.color].endLane)
        if nextSwitch and not self.pending_switch and not onASwitch and self.gameTick > 5:
            self.msg('switchLane', nextSwitch)
            self.pending_switch = True
            switchInfo = self.car.simVersion.nextSwitchLaneAndPiece(self.car.carList[self.color].endLane)
            self.car.simVersion.pendingSwitch = (switchInfo[1], self.car.carList[self.color].endLane, switchInfo[0])

        elif self.car.carList[self.color].hasTurbo and (self.at_last_straightaway(diff=0) or self.car.carList[self.color].pieceIndex == self.track_tracker.longestStraightStart):
            self.send_turbo()
        else:
            #millis2 = int(round(time.time() * 1000))
            #duration2 = int(round(time.time() * 1000)) - millis
            #if duration2 > 100:
            #    logA(("Over 100ms @", self.gameTick))

            millis2 = int(round(time.time() * 1000))
            
            mst = self.car.simVersion.findMaxSafeThrottle()
            duration2 = int(round(time.time() * 1000)) - millis
            if duration2 > 100:
                logA(("Finding max safe throttle :: over 100ms @", self.gameTick))


            if mst > 1.0:
                mst = 1.0

            if self.at_last_straightaway(diff=0):
                mst = 1.0
            self.throttle(mst)
            duration = int(round(time.time() * 1000)) - millis
            if duration > 100:
                logA(("Position processing:: over 100ms @", self.gameTick))


    def on_crash(self, data):
        name = data.get('name', "unknown")
        color = data.get('color', "unknown")
        if name == self.name and color == self.color:
            logA("We crashed at {0}".format(self.car.carList[self.color].pieceIndex))
            self.pending_switch = False
        else:
            logA("{0} crashed".format(name))
        self.car.carList[color].isCrashed = True
        self.car.on_car_changed()
        self.ping()

    def on_dnf(self, data):
        color = data.get('car', {}).get('color', False)
        logCM("DNF message: {0}".format(color))
        if color:
            self.car.carList[color].dnf = True

        self.car.on_car_changed()
        self.ping()

    def on_spawn(self, data):
        name = data.get('name', "unknown")
        color = data.get('color', "unknown")
        if name == self.name and color == self.color:
            logA("We spawned at {0}".format(self.car.carList[self.color].pieceIndex))
            self.pending_switch = False
        else:
            logA("{0} spawned".format(name))
        self.car.carList[color].isCrashed = False
        self.car.on_car_changed()
        self.ping()

    def on_game_end(self, data):
        logA("Race ended")
        logA(pprint.PrettyPrinter().pformat(data))
        self.flush_logging_buffer()
        self.ping()

    def on_error(self, data):
        logA("Error: {0}".format(data))
        self.ping()

    def on_your_car(self, data):
        self.gameId = data['gameId']
        self.name = data['name']
        self.color = data['color']

        logA("Setting Car Name: {0}, Color: {1}".format(self.name, self.color))
        self.ping()

    def on_lap_finished(self, data):
        name = data.get('car', {}).get("name", "unknown")
        color = data.get('car', {}).get("color", "unknown")
        lap = data.get('lapTime', {}).get("lap", -1)
        lapTime = data.get('lapTime', {}).get("millis", "unknown")
        if self.name == name and self.color == color:
            logA("We finished lap {0} in {1}".format(lap+1,lapTime))
        else:
            logA("{0} car finished lap {1} in {2}".format(color,lap+1,lapTime))
        self.ping()

    def on_finish(self, data):
        #self.run_compressor.encode_telemetry()
        name = data.get('name', "unknown")
        color = data.get('color', "unknown")
        if self.name == name and self.color == color:
            logA("We finished!")
        else:
            logA("{0} car finished...".format(color))

        self.car.carList[self.color].finished = True
        self.car.on_car_changed()
        self.ping()

    def on_game_init(self, data):
        #Gather some data from the inital load
        #logO(pprint.PrettyPrinter().pformat(data))
        self.track_tracker = Track.Track(data['race']['track'])
        
        self.run_compressor = Telemetry.InfoDumper(data['race']['cars'])

        for c in data['race']['cars']:
            if c['id']['color'] == self.color:
                self.car = Telemetry.CarTracker(self.color, self.track_tracker)
                self.car.color = self.color
                break
        else:
            self.car = Telemetry.CarTracker(self.color, self.track_tracker)
            logA("Dude, where's our car?")

        self.total_laps = data['race']['raceSession'].get('laps', 3)

        self.ping()

    def flush_logging_buffer(self):
        tmp = self.logging_buffer
        self.logging_buffer = []

        try:
            message = {
                'gameId': self.gameId,
                'events': tmp
            }
            self.session.post('http://real.hackcasual.io:8000/events', data=json.dumps(message), timeout=.1)
        except:
            self.logging_buffer += tmp

    def on_tournament_end(self, data):
        self.stopRunning = True
        self.car.physics_conn.send(("done",))

    def always_send(self, conn, socket):
        while True:
            if conn.poll():
                msg = conn.recv()
                if msg == "done":
                    break
                #logA("Sending response")
                socket.send(msg)
            else:
                #logA("Didn't have a response ready, going with a ping")
                socket.send((json.dumps({"msgType": "throttle", "data": 0.4})) + "\n")
            for x in range(0,70):
                if not conn.poll():
                    time.sleep(0.01)
                else:
                    break


    def off_process_msg_loop(self, conn, socket, socket_file):
        my_conn, sender_conn = Pipe()
        p = Process(target=self.always_send, args=(sender_conn,self.socket))
        p.start()


        dropPositionUpdates = 0
        stopIt = False
        msg = None
        while not stopIt:
            response = None
            if conn.poll(0.700):
                # We will drop messages to the server until we're caught up
                response = conn.recv()
                if response and response[0] == "done":
                    break

            if response:
                dropPositionUpdates = 0
                if response[0] == "server":
                    my_conn.send(response[1] + "\n")
                elif respone[0] == "done":
                    break
            elif "gameTick" in msg:
                #logA(("Didn't get a response soon enough, throttling safe"))
                my_conn.send((json.dumps({"msgType": "throttle", "data": 0.4})) + "\n")
                dropPositionUpdates += 1

            
            line = socket_file.readline()

            if not line:
                break

            msg = json.loads(line)

            if msg['msgType'] != 'carPositions' or dropPositionUpdates < 2:
                conn.send(("msg",msg, line))

        my_conn.send("done")    
        conn.send(("done",))


    def msg_loop(self):
        msg_map = {
            'gameInit' : self.on_game_init,
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions_bump_nerdz,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'yourCar': self.on_your_car,
            'finish': self.on_finish,
            'lapFinished': self.on_lap_finished,
            'dnf': self.on_dnf,
            'spawn': self.on_spawn,
            'turboAvailable': self.on_turbo_available,
            'turboStart': self.on_turbo_start,
            'turboEnd': self.on_turbo_end,
            'tournamentEnd': self.on_tournament_end
        }
        
        msg = self.remote_conn.recv()
        while msg[0] != "done" and not self.stopRunning:
            if self.emit_host:
                try:
                    self.session.post('http://%s/receiver' % self.emit_host, data=msg[2], timeout=.1)
                except:
                    pass

            millis = int(round(time.time() * 1000))
            msg = msg[1]

            self.message_history.append(msg)

            self.gameTick = msg.get('gameTick', -1)

            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                if msg_type == 'yourCar':
                    data['gameId'] = msg['gameId']
                msg_map[msg_type](data)
            else:
                logA("Got {0} at time {1}".format(msg_type, self.gameTick))
                logA(pprint.PrettyPrinter().pformat(data))
                self.ping()

            duration = int(round(time.time() * 1000)) - millis
            if duration > 500:
                logA(("Over 500ms @", self.gameTick))
            msg = self.remote_conn.recv()
        self.remote_conn.send(("done",))

    def at_last_straightaway(self, diff = 0):
        #If we have passed the last curve, book it!
        if self.car.carList[self.color].lap == self.total_laps - 1 and self.track_tracker.lastCurve - diff < self.car.carList[self.color].pieceIndex:
            return True
        else:
            return False

if __name__ == "__main__":
    if len(sys.argv) < 5:
        print("Usage: ./run host port botname botkey")
        print("Usage: ./run host port botname botkey serverhost")
        print("Usage: ./run host port botname botkey create|join track password carcount")
        print("Usage: ./run host port botname botkey create|join track password carcount serverhost")
    else:
        host, port, name, key = sys.argv[1:5]
        emit_host=None
        track = None
        password = None
        carcount = None
        create = False
        join = False
        if len(sys.argv) == 10 or len(sys.argv) == 6:
            import requests
            emit_host = sys.argv[len(sys.argv) - 1]
            print("emitting to " + emit_host)
        if len(sys.argv) >= 9:
            track = sys.argv[6]
            password = sys.argv[7]
            carcount = sys.argv[8]
            if sys.argv[5] == 'create':
                create = True
            else:
                join = True
            print("mode={0}, track={1}, password={2}, carcount={3}".format(*sys.argv[5:9]))
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key, create=create, join=join, track=track, password=password, carcount=carcount, emit_host=emit_host)
        bot.run()
