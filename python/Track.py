import math
import json
from Logging import *

class Track(object):
    def __init__(self, trackData):
        self.trackData = trackData

        # The last curve piece would be useful
        self.lastCurve = -1

        # The last curve piece would be useful
        self.longestStraightDistance = -1
        self.longestStraightStart = -1

        # Figure out which lane is on the left and which is on the right
        self.leftLane = None
        self.rightLane = None

        # A list of all the switches
        self.switches = []

        # Track the distance along the track as a map of lanes        
        self.pieceOffsets = {}
        self.laneDistances = {}

        self.curveSwitchLength = {}
        self.straightSwitchLengths = {}
        # The number of pieces on the track
        self.numberOfPieces = len(self.trackData['pieces']) - 1

        # Create a "virtual" lane for uniform position
        self.trackData['lanes'].append({ 'distanceFromCenter': 0, 'index': -1 })

        # Go through each lane, and each piece given that lane
        for lane in self.trackData['lanes']:
            self.pieceOffsets[lane['index']] = []
            self.laneDistances[lane['index']] = lane['distanceFromCenter']

            offsetAcc = 0

            for piece in trackData['pieces']:
                if 'angle' in piece:
                    offsetAcc += 2 * math.pi * (piece['radius'] +
                                                math.copysign(1, piece['angle']) * -1.0 * self.laneDistances[
                                                    lane['index']]) * math.fabs(piece['angle']) / 360.0
                else:
                    offsetAcc += piece['length']
                self.pieceOffsets[lane['index']].append(offsetAcc)


        pieceId = 0
        firstCurve = False
        firstStraightAfterCurve = -1
        for piece in trackData['pieces']:
            if 'angle' in piece and 'switch' in piece:
                if firstStraightAfterCurve < 0:
                    firstCurve = True
                self._compute_curved_transitions(pieceId)
            elif firstCurve == True:
                firstStraightAfterCurve = pieceId
                firstCurve = False
            pieceId += 1

        self.first_straight_after_curve = int(len(piece) / 2)

        # Go through each piece
        position = 0

        for piece in self.trackData['pieces']:
            if 'angle' in piece:
                self.lastCurve = position

                # We are at the first lane, if it is left, the left lane is the shortest, etc...
                if not self.leftLane or not self.rightLane:
                    shortestDistance = None
                    shortestLane = None
                    longestDistance = None
                    longestLane = None
                    for lane in self.trackData['lanes']:
                        distance = self.get_distance_traveled_in_lane(position - 1, position, lane['index'])
                        if not shortestDistance or distance < shortestDistance:
                            shortestDistance = distance
                            shortestLane = lane
                        if not longestDistance or distance > longestDistance:
                            longestDistance = distance
                            longestLane = lane

                    # Positive means we are turning right, so right is the shortest lane
                    if piece['angle'] > 0:
                        self.rightLane = shortestLane
                        self.leftLane = longestLane
                    else:
                        self.rightLane = longestLane
                        self.leftLane = shortestLane

            if 'switch' in piece:
                self.switches.append(position)
            position += 1

        # Now that we have the last curve, go through and find the longest straight away
        # This will help us decide when to use a turbo perhaps
        position = self.lastCurve + 1
        wasCurve = True
        lastStart = 0
        distance = 0

        while position != self.lastCurve:
            if position > self.numberOfPieces:
                position = 0

            atCurve = False
            if 'angle' in self.trackData['pieces'][position]:
                atCurve = True
            else:
                distance = distance + 1

            # we just started a straight away 
            if wasCurve and not atCurve:
                lastStart = position
                distance = 1

            # we just ended a straight away 
            if atCurve and not wasCurve:
                if distance > self.longestStraightDistance:
                    self.longestStraightDistance = distance
                    self.longestStraightStart = lastStart

            wasCurve = atCurve
            position = position + 1

    def on_same_straightaway(self, behindPiece, frontPiece):
        # If there is no angle between the two pieces, then we are on the same straightaway
        if 'angle' in self.trackData['pieces'][behindPiece] or 'angle' in self.trackData['pieces'][frontPiece]:
            return False

        if behindPiece == frontPiece:
            return True

        index = behindPiece
        while index != frontPiece:
            index = index + 1
            if index > self.numberOfPieces:
                index = 0

            if 'angle' in self.trackData['pieces'][index]:
                return False

        return True

    def get_piece_id_by_offset(self, pieceId, delta):
        newId = pieceId + delta
        newId += len(self.pieceOffsets[-1]) * 10
        newId %= len(self.pieceOffsets[-1])
        return newId

    def get_radius_from_pos(self, pos):

        pieceId = pos['pieceIndex']

        piece = self.trackData['pieces'][pieceId]

        if 'radius' not in piece:
            return -1

        if pos['lane']['startLaneIndex'] == pos['lane']['endLaneIndex']:
            return self.get_radius(pieceId, pos['lane']['startLaneIndex'])

        pieceLength = self.curveSwitchLength[(pieceId, pos['lane']['startLaneIndex'], pos['lane']['endLaneIndex'])]
        endWeight = pos['inPieceDistance'] / pieceLength
        startWeight = 1.0 - endWeight

        radStart = self.get_radius(pieceId, pos['lane']['startLaneIndex'])
        radEnd = self.get_radius(pieceId, pos['lane']['endLaneIndex'])

        return radStart * startWeight + radEnd * endWeight

    def get_radius(self, pieceId, lane):

        piece = self.trackData['pieces'][pieceId]

        if 'radius' not in piece:
            return -1

        return (piece['radius'] + math.copysign(1, piece['angle']) * -1.0 * self.laneDistances[lane])

    def get_piece(self, pieceId):
        return self.trackData['pieces'][pieceId]

    def get_length(self, pieceId, lane):
        if pieceId == 0:
            return self.pieceOffsets[lane][0]
        return self.pieceOffsets[lane][pieceId] - self.pieceOffsets[lane][pieceId - 1]

    def get_curve_type(self, pieceId):
        piece = self.get_piece(pieceId)
        if 'angle' not in piece:
            return 0
        return math.copysign(1, piece['angle'])

    def get_curve_angle(self, pieceId, lane, distance, angle):
        if 'angle' not in self.trackData['pieces'][pieceId]:
            return 0

        piece = self.trackData['pieces'][pieceId]

        circum = 2 * math.pi * (piece['radius'] + math.copysign(1, piece['angle']) * -1.0 * self.laneDistances[lane])
        
        twistAngle = (math.pi - (distance / circum)) / 2

        tA2 = (distance/circum) * math.pi * 2

        curAngle = (angle * math.pi) / 180.0

        r2d = 180.0 / math.pi

        #print ("C: %f D: %f TA: %f CA: %f S: %f -S: %f" % (circum, distance, tA2 * r2d, curAngle * r2d, math.sin(tA2), math.cos(-1.0 * tA2)))

        return tA2 * r2d

    # Returns a percentage position independent of lap and lane
    # This isn't super accurate on curves, but fuck it
    def track_position(self, pos):
        dist = 0
        if pos['pieceIndex'] > 0:
            dist += self.pieceOffsets[-1][pos['pieceIndex'] - 1]
        dist += pos['inPieceDistance']

        return dist / self.pieceOffsets[-1][-1]

    def track_position_i(self, pi, offset):
        dist = 0
        if pi > 0:
            dist += self.pieceOffsets[-1][pi - 1]
        dist += offset

        return dist / self.pieceOffsets[-1][-1]

    def _get_switched_straight_distance(self, length, width):
        if (length, width) in self.straightSwitchLengths:
            return self.straightSwitchLengths[(length, width)]

        # Hermetic interpolation seems to match the length of the switch piece pretty well
        lastX = 0
        lastY = 0
        totalLen = 0
        while lastX < length:
            cX = lastX + 0.1
            cY = (3 * (cX / (length * 1.0)) ** 2 - 2 * (cX / (length * 1.0)) ** 3) * width
            totalLen += math.sqrt((cX - lastX) ** 2 + (cY - lastY) ** 2)
            lastX = cX
            lastY = cY

        self.straightSwitchLengths[(length, width)] = totalLen

        return totalLen

    def _get_switched_curve_distance(self, startRad, endRad, ang):
        totalLen = 0

        if ang < 0.0:
            startRad, endRad = endRad, startRad
            ang *= -1.0


        mx = ang * math.pi / 180.0

        r = 0.0

        lastX = 1.0 * startRad
        lastY = 0.0

        while r < mx:
            r += 0.1

            sW = (mx - r) / mx
            eW = r / mx
            cR = startRad * sW + endRad * eW

            cX = math.cos(r) * cR
            cY = math.sin(r) * cR

            totalLen += math.sqrt((cX - lastX) ** 2 + (cY - lastY) ** 2)
            lastY = cY
            lastX = cX

        return totalLen

    def _compute_curved_transitions(self, pieceId):
        piece = self.get_piece(pieceId)
        # Filter out our virtual position lane
        realLanes = [self.laneDistances[x] for x in self.laneDistances.keys() if x != -1]

        # Get all the distances in sorted order, go through this forward and back
        distances = sorted(realLanes)

        transitions = []

        for i in range (0, len(distances) - 1):
            laneIndex1 = [x for x in self.laneDistances.keys() if x != -1 and self.laneDistances[x] == distances[i]][0]
            laneIndex2 = [x for x in self.laneDistances.keys() if x != -1 and self.laneDistances[x] == distances[i + 1]][0]

            rad1 = piece['radius'] + distances[i]
            rad2 = piece['radius'] + distances[i + 1]

            transitions.append((laneIndex1, laneIndex2, rad1, rad2))
            
            # Now do it for the reverse transition
            transitions.append((laneIndex2, laneIndex1, rad2, rad1))

        for t in transitions:
            self.curveSwitchLength[(pieceId, t[0], t[1])] = self._get_switched_curve_distance(t[2], t[3], piece['angle'])



    def _get_distance(self, pos, lane):
        dist = self.pieceOffsets[lane][-1] * pos['lap']
        if pos['pieceIndex'] > 0:
            dist += self.pieceOffsets[lane][pos['pieceIndex'] - 1]
        dist += pos['inPieceDistance']

        return dist

    def _get_piece_length_from_pos(self, pos):
        piece = self.get_piece(pos['pieceIndex'])
        if pos['lane']['startLaneIndex'] == pos['lane']['endLaneIndex']:
            if pos['pieceIndex'] == 0:
                return self.pieceOffsets[pos['lane']['startLaneIndex']][0]
            else:
                return self.pieceOffsets[pos['lane']['startLaneIndex']][pos['pieceIndex']] - self.pieceOffsets[pos['lane']['startLaneIndex']][pos['pieceIndex'] - 1]
            
        lane1 = pos['lane']['startLaneIndex']
        lane2 = pos['lane']['endLaneIndex']

        if 'angle' not in piece:
            return self._get_switched_straight_distance(piece['length'], math.fabs(self.laneDistances[lane1] - self.laneDistances[lane2]))
        else:
            return self.curveSwitchLength[(pos['pieceIndex'], lane1, lane2)]



    def get_distance_traveled(self, oldPos, newPos):
        if oldPos['pieceIndex'] == newPos['pieceIndex']:
            return newPos['inPieceDistance'] - oldPos['inPieceDistance']

        lastPieceLen = self._get_piece_length_from_pos(oldPos)
        lastPieceTravel = lastPieceLen - oldPos['inPieceDistance']
        
        return newPos['inPieceDistance'] + lastPieceTravel

    def get_distance_traveled_in_lane(self, oldPos, newPos, lane):
        # Given a lane index, find the distance between two positions
        if oldPos > newPos:
            # We are searching over a finish line
            return self.pieceOffsets[lane][newPos] + self.pieceOffsets[lane][-1] - self.pieceOffsets[lane][oldPos]
        else:
            return self.pieceOffsets[lane][newPos] - self.pieceOffsets[lane][oldPos]

    def get_next_switch_index(self, pieceIndex, max_lookahead=None):
        # Get's the next switch given a current piece index
        value = -1
        for index in self.switches:
            if index > pieceIndex:
                value = index
                break
        if value == -1 and len(self.switches) > 0:
            value = self.switches[0]
        if max_lookahead and value > max_lookahead + pieceIndex:
            return None
        else:
            return value

    def get_next_turn_direction(self, pieceIndex):
        i = pieceIndex + 1
        curve_type = 0
        while i < len(self.trackData['pieces']) and i <= pieceIndex + 2:
            curve_type = self.get_curve_type(i)
            if curve_type != 0:
                break
            i += 1
        return curve_type

import unittest

class TestTrack(unittest.TestCase):
    def test_track_len(self):
        td = json.load(open('../tracks/keimola.json'))
        tt = Track(td['track'])

        # Lane 0 should be longer
        self.assertTrue(tt.pieceOffsets[0] > tt.pieceOffsets[1])

        #Lane 1 total length
        self.assertAlmostEqual(tt.pieceOffsets[1][-1], 3374.1237390820806)

        #Lane 0 total length
        self.assertAlmostEqual(tt.pieceOffsets[0][-1], 3499.787445225672)


if __name__ == '__main__':
    unittest.main()