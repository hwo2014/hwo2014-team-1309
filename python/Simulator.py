import math
from Track import Track
from Tracks import KEIMOLA
import random
import json
from Logging import *

class PieceBooster(object):
    def __init__(self):
        self.pieceMaxes = {}
        self.velocityMaxes = {}
        self.stopReports = True

    def report(self, piece, angle, velocity):
        # Hack for now, need to properly record the target angle
        if self.stopReports:
            return

        if piece not in self.pieceMaxes:
            self.pieceMaxes[piece] = math.fabs(angle)
            self.velocityMaxes[piece] = math.fabs(velocity)
            return

        if math.fabs(angle) > self.pieceMaxes[piece]:
            self.pieceMaxes[piece] = math.fabs(angle)
        if math.fabs(velocity) > self.velocityMaxes[piece]:
            self.velocityMaxes[piece] = math.fabs(velocity)




    # Tells the piece tracker to 
    def retarget(self, piece, angle):
        pass


    def stop(self):
        self.stopReports = True


PIECE_BOOSTER = PieceBooster()

class SimuPack(object):
    def __init__(self, depth):
        self.history = []
        self.depth = depth

    def tick(self, throttle, track, lane, piece, offset, v, a, av, tc, lap):
        for s in self.history:
            s.tick(throttle)

        newSim = SimCar(track, lane)
        newSim.offset = offset
        newSim.piece = piece
        newSim.v = v
        newSim.a = a
        newSim.av = av
        newSim.tc = 0
        newSim.lap = lap

        self.history.append(newSim)

        if len(self.history) > self.depth:
            self.history = self.history[1:]

    def get_tail(self):
        return self.history[0]

class WORLDPHYSICS: 
    sK = -0.09047056
    dK = -0.00124436
    centA = 2.75507024
    centB = -0.82197057
    velocity = 0.98
    goForIt = False
    reallyGoForIt = False

    dangerZone = set()
    crashPoints = set()

    # Represents the largest angle we've seen prior to computing physics properties
    maxSeenAngle = -1

class SimCar(object):
    sK = -0.0075
    dK = -0.1

    def __init__(self, track, lane):
        self.track = track
        self.endLane = lane
        self.startLane = lane
        self.piece = 0
        self.offset = 0
        self.v = 0
        self.a = 0
        self.av = 0
        self.tc = 0
        self.lap = 0
        self.dead = False
        self.secret_id = random.random()
        self.turboMult = 1.0
        self.turboDuration = 30
        self.turboStartTick = -1
        self.pendingSwitch = None

    def copy(self):
        c = SimCar(self.track, self.startLane)
        c.endLane = self.endLane
        c.piece = self.piece
        c.offset = self.offset
        c.v = self.v
        c.a = self.a
        c.av = self.av
        c.tc = self.tc
        c.lap = self.lap
        c.turboMult = self.turboMult
        c.turboDuration = self.turboDuration
        c.turboStartTick = self.turboStartTick
        c.dead = False
        c.pendingSwitch = self.pendingSwitch

        return c
    
    def _get_cornering_force(self, rad, v):
        return max(WORLDPHYSICS.centA * (self.v ** 2.0 / rad) + WORLDPHYSICS.centB, 0)

    def _get_restitution_force(self):
        return ((self.av * (WORLDPHYSICS.sK + WORLDPHYSICS.dK * self.v)) + self.a * (WORLDPHYSICS.dK * self.v))

    # Converts this car's position to the format Track uses
    def _get_position(self):
        fakePos = {}
        fakePos['pieceIndex'] = self.piece
        fakePos['inPieceDistance'] = self.offset
        fakePos['lane'] = {}
        fakePos['lane']['startLaneIndex'] = self.startLane
        fakePos['lane']['endLaneIndex'] = self.endLane

        return fakePos

    def get_angular_force(self):
        cT = self.track.get_curve_type(self.piece)
        rF = self._get_restitution_force()

        rad = self.track.get_radius_from_pos(self._get_position())

        return self._get_restitution_force() + self._get_cornering_force(rad, self.v) * cT

    def tick(self, throttle):
        if (throttle > 1):
            throttle = 1
        if (throttle < 0):
            throttle = 0

        t = throttle * 10.0 * self.turboMult


        self.v = t - WORLDPHYSICS.velocity * (t - self.v)
        self.av += self.get_angular_force()
 
        self.a += self.av
        self.offset += self.v
        self.tc+=1

        if self.turboMult != 1.0:
            if self.tc > self.turboStartTick + self.turboDuration:
                self.turboMult = 1.0


        curPieceLen = self.track._get_piece_length_from_pos(self._get_position())

        if self.offset > curPieceLen:
            self.offset -= curPieceLen
            self.piece += 1
            self.startLane = self.endLane

            self.piece %= len(self.track.pieceOffsets[-1])
            if self.piece == 0:
                self.lap += 1

            if self.pendingSwitch and self.pendingSwitch[0] == self.piece:
                self.startLane = self.pendingSwitch[1]
                self.endLane = self.pendingSwitch[2]

        targetAngle = 35.0

        
        PIECE_BOOSTER.stopReports = False



        if WORLDPHYSICS.goForIt and self.lap < 2:
            WORLDPHYSICS.reallyGoForIt = True            

        if WORLDPHYSICS.reallyGoForIt:
            targetAngle = 45
        else:
            targetAngle = 35

        if self.piece in WORLDPHYSICS.crashPoints:
            targetAngle = 20

        if self.piece + 1 in WORLDPHYSICS.crashPoints:
            targetAngle = 20

        if not WORLDPHYSICS.goForIt:
            targetAngle = 20
        if self.turboMult != 1.0:
            targetAngle = 8.0

        if self.piece in WORLDPHYSICS.dangerZone:
            targetAngle = 30

        if self.piece + 1 in WORLDPHYSICS.dangerZone:
            targetAngle = 30

        if math.fabs(self.a) > targetAngle:
            self.dead = True


        #TODO: The number of laps for the run should be set
        #Ignore any danger after our last lap
        #if self.lap > 2:
        #    self.dead = False


    def _throttleTweak(self, t, i, max):
        return t * ((max - i) * 1.0 / max)


    def findMaxSafeThrottle(self):
        high = 1.1
        low = 0.15
        maxAhead = 0
        for t in range(0,8):
            mP = (high + low) / 2
            
            cT = self.copy()

            for i in range(0, 50):
                cT.tick(self._throttleTweak(mP, i, 50))
                if cT.dead:
                     # Death is imminent
                    if i > maxAhead:
                        maxAhead = i
                    high = mP
                    break
            else:
                low = mP
                maxAhead = 20
        mP += 0.05
        if mP > 1.0:
            mP = 1.0
        if maxAhead < 15:
            return 0.005

        return mP

    def nextSwitchDirection(self, current_lane, desired_lane=None):
        # Pass in the optional desired_lane to tell you how to get to a desired lane
        # Otherwise, the shortest lane will be picked
        next_switch = self.track.get_next_switch_index(self.piece, 2)
        if not next_switch:
            return None

        shortestLane = current_lane
        dir = None
        # Since 0 is valid, be explicit with our check
        if desired_lane != None and desired_lane != False:
            # We have a desired lane, get to it
            shortestLane = desired_lane
        else:
            # Just get to the shortest
            end_switch = self.track.get_next_switch_index(next_switch)
            if not end_switch:
                return None

            # Get the length of each track between switches, and pick the shortest one
            shortestDistance = self.track.get_distance_traveled_in_lane(next_switch + 1, end_switch - 1, current_lane)
            for lane in self.track.trackData['lanes']:
                if lane['index'] != -1 and lane['index'] != current_lane:
                    distance = self.track.get_distance_traveled_in_lane(next_switch + 1, end_switch - 1, lane['index'])
                    if not shortestDistance or distance < shortestDistance:
                        shortestDistance = distance
                        shortestLane = lane['index']

        rightMost = self.track.rightLane.get('index')
        leftMost = self.track.leftLane.get('index')

        # since we don't know what direction the lane we want is, see if it is between us and the left or us and the right
        if (shortestLane > current_lane and shortestLane <= rightMost) or (shortestLane < current_lane and shortestLane >= rightMost):
            dir = 'Right'
        elif (shortestLane > current_lane and shortestLane <= leftMost) or (shortestLane < current_lane and shortestLane >= leftMost):
            dir = 'Left'

        #print('leftLane: %d, rightLane %d, shortestLane: %d, shortestDistance %d, myLane: %d, switchDir: %s' % (leftMost, rightMost, shortestLane, shortestDistance, current_lane, dir))
        return dir

    def nextSwitchLaneAndPiece(self, current_lane, desired_lane=None):
        # Pass in the optional desired_lane to tell you how to get to a desired lane
        # Otherwise, the shortest lane will be picked
        next_switch = self.track.get_next_switch_index(self.piece, 2)
        if not next_switch:
            return None

        shortestLane = current_lane
        dir = None
        # Since 0 is valid, be explicit with our check
        if desired_lane != None and desired_lane != False:
            # We have a desired lane, get to it
            shortestLane = desired_lane
        else:
            # Just get to the shortest
            end_switch = self.track.get_next_switch_index(next_switch)
            if not end_switch:
                return None

            # Get the length of each track between switches, and pick the shortest one
            shortestDistance = self.track.get_distance_traveled_in_lane(next_switch + 1, end_switch - 1, current_lane)
            for lane in self.track.trackData['lanes']:
                if lane['index'] != -1 and lane['index'] != current_lane:
                    distance = self.track.get_distance_traveled_in_lane(next_switch + 1, end_switch - 1, lane['index'])
                    if not shortestDistance or distance < shortestDistance:
                        shortestDistance = distance
                        shortestLane = lane['index']

        return (shortestLane, next_switch)
