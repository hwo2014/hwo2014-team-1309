import math
import sys
import json
from Simulator import SimCar, SimuPack, PIECE_BOOSTER, WORLDPHYSICS
from Track import Track
from Logging import *
from cStringIO import StringIO
from varlenint import full_encode as vl_encode
from varlenint import decode as vl_decode
import bz2
import base91
import copy
from scipy.optimize import minimize
import numpy as np

"""
Keeps track of an individual car's telemetry in a delta compressed format
"""
class CarDeltaInfo(object):
    def __init__(self):
        # These are the only bits that need to be float encoded
        self.lastAngle = 0
        self.inPieceDistance = 0

        # Deltas on these can be represented with ints directly
        self.lastPieceIndex = 0
        self.startLane = 0
        self.endLane = 0
        self.lap = 0
        self.crashed = 0

class InfoDumper(object):
    def __init__(self, carData):
        self.cars = {}

        self.angleChange = {}
        self.distanceChange = {}
        self.pieceIndexChange = {}
        self.startLaneChange = {}
        self.endLaneChange = {}
        self.lapChange = {}
        self.crashChange = {}

        self.orderedColors = []
        for c in carData:
            color = c['id']['color']
            self.orderedColors.append(color)
            self.cars[color] = CarDeltaInfo()
            self.angleChange[color] = []
            self.distanceChange[color] = []
            self.pieceIndexChange[color] = []
            self.startLaneChange[color] = []
            self.endLaneChange[color] = []
            self.lapChange[color] = []
            self.crashChange[color] = []

        self.ticks = []
        self.lastTick = 0
        self.crashed = set()

    # Zig Zag encoding: http://en.wikipedia.org/wiki/Zigzag_code
    @staticmethod
    def _de_zig_zag(delta):
        return (delta >> 1) ^ (-(delta & 1))

    # Just incase a track has more than 127 pieces better encode these as shorts
    @staticmethod
    def _zig_zag(v):
        return (v << 1) ^ (v >> 31)

    @staticmethod
    def _decode_angle_delta(delta):
        signed = InfoDumper._de_zig_zag(delta)
        signed *= 360.0
        signed /= 2**15

        return signed

    @staticmethod
    def _decode_distance_delta(delta):
        signed = InfoDumper._de_zig_zag(delta)
        signed *= 1.0
        signed *= 2**10
        signed /= 2**23

        return signed

    def on_car_positions(self, data, tick):
        for c in data:
            color = c['id']['color']
            car = self.cars[color]

            angleDelta = c['angle'] - car.lastAngle
            pieceDistanceDelta = c['piecePosition']['inPieceDistance'] - car.inPieceDistance

            pieceIndexDelta = InfoDumper._zig_zag(c['piecePosition']['pieceIndex'] - car.lastPieceIndex)
            startLaneDelta = InfoDumper._zig_zag(c['piecePosition']['lane']['startLaneIndex'] - car.startLane)
            endLaneDelta = InfoDumper._zig_zag(c['piecePosition']['lane']['endLaneIndex'] - car.endLane)
            lapDelta = InfoDumper._zig_zag(c['piecePosition']['lap'] - car.lap)

            car.lastPieceIndex = c['piecePosition']['pieceIndex']
            car.startLane = c['piecePosition']['lane']['startLaneIndex']
            car.endLane = c['piecePosition']['lane']['endLaneIndex']
            car.lap = c['piecePosition']['lap']


            isCrashed = 0

            if color in self.crashed:
                isCrashed = 1

            crashDelta = InfoDumper._zig_zag(isCrashed - car.crashed)

            car.crashed = isCrashed

            angleDelta *= 2**15
            angleDelta /= 360.0
            angleDelta = int(angleDelta)
            angleDelta = (angleDelta << 1) ^ (angleDelta >> 15)
            car.lastAngle += InfoDumper._decode_angle_delta(angleDelta)

            pieceDistanceDelta *= 2**23
            pieceDistanceDelta /= 2**10
            pieceDistanceDelta = int(pieceDistanceDelta)
            pieceDistanceDelta = (pieceDistanceDelta << 1) ^ (pieceDistanceDelta >> 31)
            car.inPieceDistance += InfoDumper._decode_distance_delta(pieceDistanceDelta)

            self.angleChange[color].append(angleDelta)
            self.distanceChange[color].append(pieceDistanceDelta)
            self.pieceIndexChange[color].append(pieceIndexDelta)
            self.startLaneChange[color].append(startLaneDelta)
            self.endLaneChange[color].append(endLaneDelta)
            self.lapChange[color].append(lapDelta)
            self.crashChange[color].append(crashDelta)

        tickDelta = tick - self.lastTick
        self.lastTick = tick

        self.ticks.append(self._zig_zag(tickDelta))

    def encode_telemetry(self):
        pointCount = len(self.ticks)
        carCount = len(self.angleChange)
        out = StringIO()

        car_colors = json.dumps(self.orderedColors)
        ccLen = len(car_colors)
        out.write(vl_encode(ccLen))
        out.write(car_colors)

        out.write(vl_encode(pointCount))

        for t in self.ticks:
            out.write(vl_encode(t))

        for c in self.orderedColors:
            for x in self.angleChange[c]:
                out.write(vl_encode(x))

        for c in self.orderedColors:
            for x in self.distanceChange[c]:
                out.write(vl_encode(x))

        for c in self.orderedColors:
            for x in self.pieceIndexChange[c]:
                out.write(vl_encode(x))

        for c in self.orderedColors:
            for x in self.startLaneChange[c]:
                out.write(vl_encode(x))

        for c in self.orderedColors:
            for x in self.endLaneChange[c]:
                out.write(vl_encode(x))

        for c in self.orderedColors:
            for x in self.lapChange[c]:
                out.write(vl_encode(x))

        for c in self.orderedColors:
            for x in self.crashChange[c]:
                out.write(vl_encode(x))

        comp = base91.encode(bz2.compress(out.getvalue(), 9))

        logO("====== BEGIN TELEMETRY DATA ======")
        while len(comp) > 0:
            logO(comp[:80])
            comp = comp[80:]
        logO("======= END TELEMETRY DATA =======")


    @staticmethod
    def decode_telemetry(data):
        data = StringIO(bz2.decompress(base91.decode(data)))

        carJsonLen = vl_decode(data)
        orderedColors = json.loads(data.read(carJsonLen).decode("UTF-8"))

        carCount = len(orderedColors)

        tickCount = vl_decode(data)

        ticks = []

        angles = {}
        distances = {}
        pieces = {}
        startLanes = {}
        endLanes = {}
        laps = {}
        crashFlags = {}

        for i in range(0, carCount):
            angles[i] = []
            distances[i] = []
            pieces[i] = []
            startLanes[i] = []
            endLanes[i] = []
            laps[i] = []
            crashFlags[i] = []

        lastTick = 0
        for i in range(0, tickCount):
            tickDelta = InfoDumper._de_zig_zag(vl_decode(data))
            lastTick += tickDelta
            ticks.append(lastTick)

        for c in range(0, carCount):
            last = 0
            for i in range(0, tickCount):
                delta = InfoDumper._decode_angle_delta(vl_decode(data))
                last += delta
                angles[c].append(last)

        for c in range(0, carCount):
            last = 0
            for i in range(0, tickCount):
                delta = InfoDumper._decode_distance_delta(vl_decode(data))
                last += delta
                distances[c].append(last)

        for c in range(0, carCount):
            last = 0
            for i in range(0, tickCount):
                delta = InfoDumper._de_zig_zag(vl_decode(data))
                last += delta
                pieces[c].append(last)

        for c in range(0, carCount):
            last = 0
            for i in range(0, tickCount):
                delta = InfoDumper._de_zig_zag(vl_decode(data))
                last += delta
                startLanes[c].append(last)

        for c in range(0, carCount):
            last = 0
            for i in range(0, tickCount):
                delta = InfoDumper._de_zig_zag(vl_decode(data))
                last += delta
                endLanes[c].append(last)

        for c in range(0, carCount):
            last = 0
            for i in range(0, tickCount):
                delta = InfoDumper._de_zig_zag(vl_decode(data))
                last += delta
                laps[c].append(last)

        for c in range(0, carCount):
            last = 0
            for i in range(0, tickCount):
                delta = InfoDumper._de_zig_zag(vl_decode(data))
                last += delta
                crashFlags[c].append(last)

        print(distances)

