import math
import sys
import json
from Simulator import SimCar, SimuPack, PIECE_BOOSTER, WORLDPHYSICS
from Track import Track
from Logging import *
from cStringIO import StringIO
from varlenint import full_encode as vl_encode
from varlenint import decode as vl_decode
import bz2
import base91
import copy
from scipy.optimize import minimize
import numpy as np
from RunSaver import InfoDumper
import time
from multiprocessing import Process, Pipe
#millis = int(round(time.time() * 1000))

DAMPING_K = 0.1
SPRING_K = 0.0075

def modeler_process(conn, track):
    modeler = PhysicsModeler(track)
    

    while True:
        msg = conn.recv()
        if len(modeler.positionHistory) == 6:
            t = modeler.positionHistory[3]['throttle'] * 10.0
            v1 = modeler.positionHistory[4]['car'].inPieceDistance - modeler.positionHistory[3]['car'].inPieceDistance
            v2 = modeler.positionHistory[5]['car'].inPieceDistance - modeler.positionHistory[4]['car'].inPieceDistance

            vC = (v2 - t) / (v1 - t)
            conn.send(("velocity",vC))

        if msg[0] == "throttle":
            modeler.on_throttle(msg[1])
        elif msg[0] == "car":
            modeler.on_car_update(msg[1])
        elif msg[0] == "get":
            logCV(("Good ticks: ", len(modeler.goodTicks)))
            vC = modeler.get_velocity_const()
            spring = modeler.get_spring_const()
            cent = modeler.get_centripetal_force()


            cautionPieces = set()

            data = (vC, spring[0], spring[1], cent[0], cent[1], cautionPieces)

            conn.send(("physics", data))
        elif msg[0] == "done":
            break
        elif msg[0] == "goodTick":            
            modeler.goodTicks.add(msg[1])
        elif msg[0] == "goodTick":                        
            modeler.goodTicks.remove(msg[1])

class PhysicsModeler(object):

    def __init__(self, track):
        self.pendingThrottle = 0
        self.positionHistory = []
        self.track = track
        self.springFactor = 0
        self.dampingFactor = 0
        self.goodTicks = set()

    def on_throttle(self, throttle):
        self.pendingThrottle = throttle

    def on_car_update(self, car):
        self.positionHistory.append({'throttle': self.pendingThrottle, 'car': copy.copy(car)})

    def get_velocity_const(self):
        if len(self.positionHistory) < 10:
            return 0.98

        t = self.positionHistory[6]['car'].throttle * 10
        v1 = self.positionHistory[6]['car'].inPieceDistance - self.positionHistory[5]['car'].inPieceDistance
        v2 = self.positionHistory[7]['car'].inPieceDistance - self.positionHistory[6]['car'].inPieceDistance

        vC = (v2 - t) / (v1 - t)


        #x0 = np.array([vC])
        #res = minimize(self._velocity_model, x0, method='nelder-mead',
        #        options={'xtol': 1e-8})

        #logO(res)
        return vC

    def get_spring_const(self):

        x0 = np.array([-0.00745345,-0.0985848])
        res = minimize(self._restitution_model, x0, method='nelder-mead',
                options={'xtol': 1e-8})

        self.springFactor = res.x[0]
        self.dampingFactor = res.x[1]
        logO(res)
        return res.x

    def get_centripetal_force(self):

        x0 = np.array([2.20480354,-0.68419722])
        res = minimize(self._centripetal_model, x0, method='nelder-mead',
                options={'xtol': 1e-8})

        self.cenA = res.x[0]
        self.cenB = res.x[1]
        logO(res)
        return res.x

    def _good_car(self, i):
        if i == 0:
            return False
        return (self.positionHistory[i]['car'].pieceIndex == self.positionHistory[i - 1]['car'].pieceIndex
                and self.positionHistory[i]['car'].pieceIndex == self.positionHistory[i + 1]['car'].pieceIndex
                and self.positionHistory[i]['car'].currentTick in self.goodTicks 
                and self.positionHistory[i - 1]['car'].currentTick in self.goodTicks 
                and self.positionHistory[i + 1]['car'].currentTick in self.goodTicks
                and self.positionHistory[i]['car'].currentTick + 1 == self.positionHistory[i + 1]['car'].currentTick
                and self.positionHistory[i - 1]['car'].currentTick + 1 == self.positionHistory[i]['car'].currentTick)

    def _velocity_model(self, x):

        errorAcc = 0.0

        for i in range(5, len(self.positionHistory) - 1):
            if (self.positionHistory[i]['car'].pieceIndex == self.positionHistory[i - 1]['car'].pieceIndex
                and self.positionHistory[i]['car'].pieceIndex == self.positionHistory[i + 1]['car'].pieceIndex
                and self._good_car(i) and i in self.goodTicks and i - 1 in self.goodTicks and i + 1 in self.goodTicks):
                t = self.positionHistory[i - 1]['car'].throttle * 10.0
                v1 = self.positionHistory[i]['car'].inPieceDistance - self.positionHistory[i - 1]['car'].inPieceDistance
                v2 = self.positionHistory[i + 1]['car'].inPieceDistance - self.positionHistory[i]['car'].inPieceDistance

                expected = t - x[0] * (t - v1)

                # Initial guess
                errorAcc += (expected - v2) ** 2.0

        return math.sqrt(errorAcc)

    def _restitution_model(self, x):
        totalError = 0
        pH = self.positionHistory
        for i in range(1, len(pH) - 1):

            if (self.track.get_curve_type(pH[i]['car'].pieceIndex) == 0.0 and
                self.track.get_curve_type(pH[i - 1]['car'].pieceIndex) == 0.0 and
                self.track.get_curve_type(pH[i - 2]['car'].pieceIndex) == 0.0 and
                self.track.get_curve_type(pH[i + 1]['car'].pieceIndex) == 0.0 and
                pH[i]['car'].startLane == pH[i]['car'].endLane
                and self._good_car(i)
                and i in self.goodTicks and i - 1 in self.goodTicks and i + 1 in self.goodTicks):


                aV = (pH[i]['car'].angle - pH[i - 1]['car'].angle)
                nAV = (pH[i + 1]['car'].angle - pH[i]['car'].angle)
                nA = aV + ((aV * (x[0] + x[1] * pH[i]['car'].velocity)) + (pH[i]['car'].angle) * (x[1] * pH[i]['car'].velocity))
                totalError += (nAV - nA) ** 2

        return math.sqrt(totalError)

    def _centripetal_model(self, x):
        totalError = 0
        pH = self.positionHistory
        for i in range(1, len(pH) - 1):

            if (pH[i]['car'].startLane == pH[i]['car'].endLane
                and self._good_car(i) and i in self.goodTicks and i - 1 in self.goodTicks and i + 1 in self.goodTicks):

                cT = self.track.get_curve_type(pH[i]['car'].pieceIndex)

                if cT != 0:
                    rad = self.track.get_radius(pH[i]['car'].pieceIndex, pH[i]['car'].endLane)
                

                aV = (pH[i]['car'].angle - pH[i - 1]['car'].angle)
                nAV = (pH[i + 1]['car'].angle - pH[i]['car'].angle)

                nA = aV + ((aV * (self.springFactor + self.dampingFactor * pH[i]['car'].velocity)) + (pH[i]['car'].angle) * (self.dampingFactor * pH[i]['car'].velocity))

                if cT != 0:
                    nA += max(x[0] * (pH[i]['car'].velocity ** 2.0 / rad) + x[1], 0) * cT

                totalError += (nAV - nA) ** 2

        return math.sqrt(totalError)

    def _computer_piece_error(self, x):
        totalError = 0
        self.piecesWithError = {}
        pH = self.positionHistory
        for i in range(1, len(pH) - 1):

            if (pH[i]['car'].startLane == pH[i]['car'].endLane
                and i in self.goodTicks and i - 1 in self.goodTicks and i + 1 in self.goodTicks):

                cT = self.track.get_curve_type(pH[i]['car'].pieceIndex)

                if cT != 0:
                    rad = self.track.get_radius(pH[i]['car'].pieceIndex, pH[i]['car'].endLane)
                

                aV = (pH[i]['car'].angle - pH[i - 1]['car'].angle)
                nAV = (pH[i + 1]['car'].angle - pH[i]['car'].angle)

                nA = aV + ((aV * (self.springFactor + self.dampingFactor * pH[i]['car'].velocity)) + (pH[i]['car'].angle) * (self.dampingFactor * pH[i]['car'].velocity))

                if cT != 0:
                    nA += max(x[0] * (pH[i]['car'].velocity ** 2.0 / rad) + x[1], 0) * cT

                totalError += (nAV - nA) ** 2

                if pH[i]['car'].pieceIndex not in self.piecesWithError:
                    self.piecesWithError[pH[i]['car'].pieceIndex] = 0

                self.piecesWithError[pH[i]['car'].pieceIndex] += (nAV - nA) ** 2

        return math.sqrt(totalError)

class CarInfo(object):
    def __init__(self):
        self.name = "foo"
        self.color = "bar"

        # These are values give to us every update
        self.angle = 0
        self.inPieceDistance = 0
        self.pieceIndex = 0
        self.startLane = 0
        self.endLane = 0
        self.lap = 0

        # These values are calculated or determined otherwise
        self.switchingLanes = False
        self.hasTurbo = False
        self.inTurbo = False
        self.isCrashed = False
        self.dnf = False
        self.finished = False
        self.turboStartTick = 0
        self.turboDuration = 0
        self.turboMultiplier = 3
        self.justSwitchedLanes = False
        self.justBumped = False
        self.currentTick = 0

        # List of the switches on a track to an array of the end lane after the switch
        self.switchesTaken = {}

        # We can bump people in our lane who are close. We can turbo into anyone in our straightaway.
        # A person can bump us if they are right behind us close
        # A turbo could happen from serveral people behind us, but on straight away
        #
        # Example:
        #
        #  ==B====C=us=D===E===
        # /                    \
        # |A                  F|
        #
        # we can bump into D
        # we can turbo into D and E if we have turbo
        # C can bump into us
        # B and C can turbo into us if they have turbo
        #
        # This is a list of car colors
        self.canBumpThem = []
        self.canTurboThem = []
        self.canBumpUs = []
        self.canTurboUs = []
        self.isTurboUs = []

        # In front in same lane CarTracker.inFrontDistance pieces
        self.inFrontOfUs = []

class CarTracker(object):
    history = []
    def __init__(self, color, track):
        self.color = color
        self.velocity = 0
        self.angularVelocity = 0
        self.predictedAngularVelocity = 0
        self.angularMomentum = 0
        self.lastPos = None
        self.track = track
        self.lastAngle = 0
        self.lastThrottle = 0
        self.lastVelocity = 0
        self.throttle = 0
        self.simVersion = SimCar(track, 0)
        self.seenAngles = set()

        # Used in car info calculations
        self.inFrontDistance = 1

        #self.telemF = open("centripetal.csv","w")

        #self.telemF.write("Tick,Ang,AngV,V,Radius\n")
        #self.telemF.write("Tick,Ang,AngV,V,Radius,CurveDirections,PieceId\n")
        #self.telemF.write("Tick, Delta, AngV, CentripTerm\n")

        self.simTracker = SimuPack(25)        
        self.lastTick = 0

        self.ignoreFirstThrottle = 0

        #Bucketize the track into 250 positions to track speed, angular velocity and angle
        self.posVars = {}

        # List of all current cars
        self.carList = {}

        # Build up the current rankings, a list of car colors
        self.rankings = []

        self.physics_conn, child_conn = Pipe()
        self.physics_runner = Process(target=modeler_process, args=(child_conn,track))
        self.physics_runner.start()

    def _update_pos_vars(self):
        pos = int(self.track.track_position(self.lastPos) * 250)

        danger = 0

        try:
            danger = self.angularVelocity / (self.track.get_curve_type(self.lastPos['pieceIndex']) * 60 - self.lastAngle)
        except:
            pass

        self.posVars[pos] = {"v": self.velocity,
            "a": self.lastAngle,
            "ad": self.angularVelocity,
            "c": self.track.get_curve_type(self.lastPos['pieceIndex']),
            "danger": danger
        }

    def get_danger_score(self):
        pos = int(self.track.track_position(self.lastPos) * 250)

        danger_acc = 0

        for i in range(0,40):
            try:
                danger_acc += (self.posVars[(pos + i) % 250]['danger'] / i) * 3.0
            except:
                pass

        return danger_acc

    def on_throttle(self, throttle):
        if self.carList[self.color].isCrashed:
            return
        if self.ignoreFirstThrottle > 2:
            if self.carList[self.color].inTurbo:
                self.simVersion.turboMult = 3.0
            else:
                self.simVersion.turboMult = 1.0
            self.simTracker.tick(self.lastThrottle,
                    self.track, self.lastPos['lane']['endLaneIndex'],
                    self.lastPos['pieceIndex'], self.lastPos['inPieceDistance'],
                    self.velocity, self.lastAngle, self.angularVelocity,
                    self.lastTick,self.lastPos['lap']
                    )
            self.simVersion.tick(self.throttle)

        self.ignoreFirstThrottle += 1
        self.lastThrottle = self.throttle
        self.throttle = throttle

        self.physics_conn.send(("throttle", throttle))

    def curve_aggression(self):
        # If our angle is decreasing, reapply throttle
        if (math.copysign(1, self.angularVelocity) != math.copysign(1, self.lastAngle) and
                math.fabs(self.angularVelocity) > 1.0 and math.fabs(self.angularVelocity) < 2.5 and 
                math.fabs(self.lastAngle) > 20.0):
            #logCV(("Getting aggressive in the curves", self.angularVelocity, self.lastAngle))
            return False
        return False

    def calculate_rankings(self):
        self.rankings = []

        for i in self.carList:
            car = self.carList[i]
            if len(self.rankings) == 0:
            # Only car?  You are in the lead!
                self.rankings.append(car.color)
            else:
                insertPosition = 0
                #More than one car, sad...
                for j in self.rankings:
                    if (self.carList[j].lap < car.lap or
                        (self.carList[j].lap == car.lap and self.carList[j].pieceIndex < car.pieceIndex) or
                        (self.carList[j].lap == car.lap and self.carList[j].pieceIndex == car.pieceIndex and self.carList[j].inPieceDistance < car.inPieceDistance)):
                        #We must be in the lead, break!
                        break
                    insertPosition = insertPosition + 1
                self.rankings.insert(insertPosition, car.color)

        # Output the new rankings:
        #rank = 1
        #for k in self.rankings:
        #    logCM("{0}) {1}".format(rank, self.carList[k].color))
        #    rank = rank + 1

    def ouput_switches_taken(self):
        for color in self.carList:
            car = self.carList[color]
            for i in car.switchesTaken:
                logCM("{0} during switch {1} was in lane {2}".format(color, i, car.switchesTaken[i]))

    def calculate_bump_danger(self):
        lastPiece = self.track.numberOfPieces
        for color in self.carList:
            us = self.carList[color]

            # Figure out if there is someone in front of us or behind us!
            self.carList[color].canBumpThem = []
            self.carList[color].canTurboThem = []
            self.carList[color].inFrontOfUs = []
            self.carList[color].canBumpUs = []
            self.carList[color].canTurboUs = []
            self.carList[color].isTurboUs = []

            if us.dnf or us.finished or us.isCrashed:
                continue

            for i in self.carList:
                car = self.carList[i]
                # Must have the same end lane, and the car must be in play
                if car.color != color and car.endLane == us.endLane and not car.dnf and not car.finished and not car.isCrashed:

                    # They can bump us if they are behind in our piece or one piece behind us
                    if ((car.pieceIndex == us.pieceIndex and car.inPieceDistance < us.inPieceDistance) or
                        (car.pieceIndex == us.pieceIndex - 1) or
                        (us.pieceIndex == 0 and car.pieceIndex == lastPiece)):
                        self.carList[color].canBumpUs.append(car.color)

                    # We can bump them if we are behind in our piece or one piece behind them
                    if ((car.pieceIndex == us.pieceIndex and car.inPieceDistance > us.inPieceDistance) or
                        (car.pieceIndex == us.pieceIndex + 1) or
                        (car.pieceIndex == 0 and us.pieceIndex == lastPiece)):
                        self.carList[color].canBumpThem.append(car.color)

                    # They can turbo into us if they are on the same straightaway
                    if self.track.on_same_straightaway(car.pieceIndex, us.pieceIndex) and not (car.pieceIndex == us.pieceIndex and car.inPieceDistance > us.inPieceDistance):
                        if car.hasTurbo:
                            self.carList[color].canTurboUs.append(car.color)
                        if car.inTurbo:
                            self.carList[color].isTurboUs.append(car.color)

                    # We can turbo into someone if we are on the same straightaway
                    if us.hasTurbo and self.track.on_same_straightaway(us.pieceIndex, car.pieceIndex)  and not (car.pieceIndex == us.pieceIndex and car.inPieceDistance < us.inPieceDistance):
                        self.carList[color].canTurboThem.append(car.color)

                    if (car.pieceIndex == us.pieceIndex and car.inPieceDistance > us.inPieceDistance) or (car.pieceIndex > us.pieceIndex and car.pieceIndex - self.inFrontDistance <= us.pieceIndex) or (car.pieceIndex <= self.inFrontDistance and us.pieceIndex + self.inFrontDistance > self.track.numberOfPieces and (us.pieceIndex + self.inFrontDistance % self.track.numberOfPieces) <= car.pieceIndex):
                        self.carList[color].inFrontOfUs.append(car.color)

            #for i in self.carList[color].canBumpThem:
            #    logCM("{0} can bump {1}".format(color, self.carList[i].color))
            #for i in self.carList[color].canTurboThem:
            #    logCM("{0} can turbo into {1}".format(color, self.carList[i].color))
            #for i in self.carList[color].canBumpUs:
            #    logCM("{0} can bump {1}".format(self.carList[i].color, color))
            #for i in self.carList[color].canTurboUs:
            #    logCM("{0} can turbo into {1}".format(self.carList[i].color, color))
            #for i in self.carList[color].isTurboUs:
            #    logCM("{0} is turbo-ing into {1}!".format(self.carList[i].color, color))
            #for i in self.carList[color].inFrontOfUs:
            #    logCM("{0} is in front of {1}!".format(self.carList[i].color, color))

    def on_car_changed(self):
        self.calculate_bump_danger()

    def on_position(self, data, tick):
        self.lastTick = tick
        # First, update all the data
        myPos = {}
        for car in data:
            carColor = car['id'].get('color',"unknown")
            if not carColor in self.carList:
                self.carList[carColor] = CarInfo()
                self.carList[carColor].name = car['id'].get('name',"unknown")
                self.carList[carColor].color = carColor

            previousPosition = self.carList[carColor].pieceIndex;

            self.carList[carColor].angle = car['angle']
            self.carList[carColor].lap = car['piecePosition']['lap']
            self.carList[carColor].startLane = car['piecePosition']['lane']['startLaneIndex']
            self.carList[carColor].endLane = car['piecePosition']['lane']['endLaneIndex']
            self.carList[carColor].pieceIndex = car['piecePosition']['pieceIndex']
            self.carList[carColor].inPieceDistance = car.get('piecePosition', {}).get('inPieceDistance', 0)

            # If we just entered a piece, see if it is a switch and figure out where we are going!
            if previousPosition != self.carList[carColor].pieceIndex and self.carList[carColor].pieceIndex in self.track.switches:
                if not self.carList[carColor].pieceIndex in self.carList[carColor].switchesTaken:
                    self.carList[carColor].switchesTaken[self.carList[carColor].pieceIndex] = []
                self.carList[carColor].switchesTaken[self.carList[carColor].pieceIndex].append(self.carList[carColor].endLane)

            laneSwitch = self.carList[carColor].switchingLanes

            # Figure out if we are currently switching lanes!
            if self.carList[carColor].startLane != self.carList[carColor].endLane:
                self.carList[carColor].switchingLanes = True
            else:
                self.carList[carColor].switchingLanes = False

            # We need to know if we have just switched lanes (but are done)
            if laneSwitch and not self.carList[carColor].switchingLanes:
                self.carList[carColor].justSwitchedLanes = True
            else:
                self.carList[carColor].justSwitchedLanes = False

            if carColor == self.color:
                myPos = car['piecePosition']

            #logCM("N: {0}, C: {1}, L: {2}, S: {3}, E: {4}, P: {5}, D: {6}, A: {7}".format(self.carList[carColor].name, self.carList[carColor].color, self.carList[carColor].lap, self.carList[carColor].startLane, self.carList[carColor].endLane, self.carList[carColor].pieceIndex, self.carList[carColor].inPieceDistance, self.carList[carColor].angle))
            #logCM("switching: {0}, hasTurbo: {1}, crashed: {2}, dnf {3}".format(self.carList[carColor].switchingLanes, self.carList[carColor].hasTurbo, self.carList[carColor].isCrashed, self.carList[carColor].dnf))

        # We have everyone's position, now recalculate the rankings
        self.calculate_rankings()
        self.calculate_bump_danger()

        # Now go through the normal stuff
        ourCar = self.carList[self.color]

        #logCV(("Reckoned throttle constant: ", self.modeler.get_velocity_const()))
        #logCV(("Reckoned spring restitution constant: ", self.modeler.get_spring_const()))

        t = self.track

        self.seenAngles.add(math.fabs(ourCar.angle))

        if ourCar.lap == 0 and ourCar.pieceIndex == self.track.first_straight_after_curve:
            WORLDPHYSICS.maxSeenAngle = max(self.seenAngles)

        ang = 0
        if self.lastPos:
            lastR = self.track.get_radius(self.lastPos['pieceIndex'], self.lastPos['lane']['endLaneIndex'])
            lct = self.track.get_curve_type(self.lastPos['pieceIndex'])
            cenF = 0

            if lastR > 1.0:
                cenF = max(3.0 * (self.lastVelocity ** 2 / lastR) - 1.0, 0) * lct

            sK = 0.0075
            dK = 0.1
            fS = (0 - self.lastAngle) * sK
            nc = self.angularVelocity + fS + dK * self.angularVelocity * -1.0

            self.lastVelocity = self.velocity
            self.velocity = t.get_distance_traveled(self.lastPos, myPos)

            # Sorry Cody, it's a hack, but otherwise I'll have to change a bunch of code
            # It's late....
            ourCar.velocity = self.simVersion.v
            PIECE_BOOSTER.report(ourCar.pieceIndex, ourCar.angle, ourCar.velocity)

            ourCar.currentTick = tick
            ourCar.throttle = self.throttle

            self.physics_conn.send(("car", ourCar))


            #if (myPos['pieceIndex'] == self.lastPos['pieceIndex'] and 'angle' in self.track.trackData['pieces'][myPos['pieceIndex']]):
            ang = self.track.get_curve_angle(ourCar.pieceIndex, ourCar.endLane, self.velocity, ourCar.angle)

            self.angularMomentum += ang
            if self.angularMomentum < 3:
                self.angularMomentum = 0
            else:
                self.angularMomentum -= 3

            self.angularMomentum *= 0.75

            self.angularVelocity = ourCar.angle - self.lastAngle
            self.lastAngle = ourCar.angle
            #self.telemF.write("Tick,Ang,AngV,V,Radius,PieceId\n")
            #print("%d,%0.4f,%0.4f,%0.4f,%0.4f,%0.4f,%d" % (tick, self.lastAngle, self.angularVelocity, self.simVersion.v, lastR, lct, self.lastPos['pieceIndex']))

        try:

            bSX = self.simTracker.get_tail()

            sDav = self.angularVelocity - bSX.av
            sDa = self.lastAngle - bSX.a

            #logCV((sDav, sDa))


        except:
            pass


            #logCV(self.simVersion.findMaxSafeThrottle())


            #logCV((myPos['pieceIndex'], sDo, sDv))

        try:

            bSX = self.simVersion

            sDv = self.velocity - bSX.v
            sDo = myPos['inPieceDistance'] - bSX.offset
            sDa = self.lastAngle - bSX.a
            sDav = self.angularVelocity - bSX.av

            if ourCar.angle > 50:
                WORLDPHYSICS.dangerZone.add(ourCar.pieceIndex)

            if ourCar.isCrashed:
                WORLDPHYSICS.crashPoints.add(ourCar.pieceIndex)

            if math.fabs(sDo) < 0.1 and not ourCar.isCrashed:
                self.physics_conn.send(("goodTick", tick))
            else:
                self.physics_conn.send(("badTick", tick - 1))
                self.physics_conn.send(("badTick", tick - 2))
            #else:
            #    logCV(">> %f  :: %d,%f,%f,%f,%f,%f, %f, %d, || %f, %f" % (self.lastAngle, tick, sDa, sDav, sDv, sDo, bSX.offset, myPos['inPieceDistance'], bSX.tc, self.velocity, bSX.v))
            # if our change is too much, we are in trouble!
            detectedBump = False
            if abs(sDv) > 0:
                if self.carList[self.color].justSwitchedLanes:
                    if abs(sDv) > 3:
                        detectedBump = True
                elif abs(sDv) > 1:
                    detectedBump = True

            if detectedBump and (len(self.carList.canBumpUs) > 0 or len(self.carList.canTurboThem) > 0):
                self.carList[self.color].justBumped = True
            else:
                self.carList[self.color].justBumped = False

            #if math.fabs(sDa) > 3:
            #logCV(">> %f  :: %d,%f,%f,%f,%f,%f, %f, %d, || %f, %f" % (self.lastAngle, tick, sDa, sDav, sDv, sDo, bSX.offset, myPos['inPieceDistance'], bSX.tc, self.velocity, bSX.v))


        except:
            pass

        self.simVersion.v = self.velocity
        self.simVersion.endLane = ourCar.endLane
        self.simVersion.startLane = ourCar.startLane
        self.simVersion.piece = ourCar.pieceIndex
        self.simVersion.offset = ourCar.inPieceDistance
        self.simVersion.a = self.lastAngle
        self.simVersion.av = self.angularVelocity
        self.simVersion.td = ourCar.turboDuration
        self.simVersion.turboStartTick = ourCar.turboStartTick

        if self.lastPos and self.lastPos['lap']  != myPos['lap'] and myPos['lap'] == 2 and not WORLDPHYSICS.goForIt:
            # Fun little python fact, if you want to create a 1-tuple it needs to be ((x,)) not ((x))
            self.physics_conn.send(("get",))

        if self.lastPos and self.lastPos['lap']  != myPos['lap'] and myPos['lap'] < 2 and WORLDPHYSICS.goForIt:
            # Fun little python fact, if you want to create a 1-tuple it needs to be ((x,)) not ((x))
            self.physics_conn.send(("get",))

        if self.lastPos and self.lastPos['lap']  != myPos['lap'] and myPos['lap'] == 5:
            # Fun little python fact, if you want to create a 1-tuple it needs to be ((x,)) not ((x))
            self.physics_conn.send(("get",))


        if self.physics_conn.poll():
            msg = self.physics_conn.recv()
            if msg[0] == "physics":
                WORLDPHYSICS.velocity = msg[1][0]
                WORLDPHYSICS.sK = msg[1][1]
                WORLDPHYSICS.dK = msg[1][2]
                WORLDPHYSICS.centA = msg[1][3]
                WORLDPHYSICS.centB = msg[1][4]
                #WORLDPHYSICS.dangerZone = msg[1][5]

                if WORLDPHYSICS.goForIt:
                    WORLDPHYSICS.reallyGoForIt = True

                WORLDPHYSICS.goForIt = True
            elif msg[0] == "velocity":
                WORLDPHYSICS.velocity = msg[1]
                logCV(("Updated velocity", msg[1]))
            else:
                logCV(("Crap on a cracker", msg))

            logCV("Got updated physics")

        self.lastPos = myPos


        self._update_pos_vars()

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print ("Usage: Telemetry <filename>")
        sys.exit(-1)

    tData = ''.join(open(sys.argv[1]).readlines())


    InfoDumper.decode_telemetry(tData)
